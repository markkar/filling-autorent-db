def to_str_bool(xs):
    if len(xs) != 5:
        xs = ['0'] * (5 - len(xs)) + xs
    ys = []
    for x in xs:
        if x == '1':
            ys.append('true')
        else:
            ys.append('false')

    return ys


f = open('sqls/demands.sql', 'w')
for i in range(1, 33):
    demands = [ch for ch in str(bin(i - 1))[2:]]
    gps, wifi, own_driver, baby_chair, air_conditioning = tuple(to_str_bool(demands))
    sqlString = f"INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning) VALUES ({i}, {gps}, {wifi}, {own_driver}, {baby_chair}, {air_conditioning});"
    f.write(sqlString + '\n')

f.close()