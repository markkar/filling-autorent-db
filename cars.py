import random
import string
import datetime


def generate_rand_str(n):
    res = ''
    for _ in range(n):
        rand_char = random.choice(string.ascii_letters)
        res += rand_char
    return res


def generate_rand_digit(n):
    res = ''
    for _ in range(n):
        rand_digit = str(random.randint(1, 9))
        res += rand_digit
    return res


def generate_rand_date(start_date, end_date):
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)

    return (start_date + datetime.timedelta(days=random_number_of_days)).strftime("%Y-%m-%d")


def generate_rand_bool(percentage):
    return random.randrange(100) < percentage


def generate_steering_side():
    if generate_rand_bool(20):
        return 'L'
    else:
        return 'R'


existed_car_numbers = set()


def generate_car_number():
    while True:
        number = generate_rand_str(1) + generate_rand_digit(3) + generate_rand_str(2) + generate_rand_digit(2)
        if number not in existed_car_numbers:
            existed_car_numbers.add(number)
            return number


car_types = ["sedan", "hatchback", "suv", "coupe", "minivan", "pickup", "van"]
transmission_types = ['auto', 'semiauto', 'manual']
drive_types = ['front', 'back', 'full']
engine_types = ['petrol', 'electric', 'diesel']
colors = ['black', 'white', 'gray', 'silver', 'blue']

f = open('sqls/cars.sql', 'w')

for i in range(1000):
    number = generate_car_number()
    model_id = random.randint(1, 1000)
    place_id = random.randint(1, 1000)
    order_id = random.choice(['Null', random.randint(1, 1000)])
    photo = '/img/' + generate_rand_str(random.choice([6, 7, 8, 9])) + '.png'
    car_type = random.choice(car_types)
    cost_per_day = round(random.uniform(150, 300), 2)
    max_passenger_number = random.randint(5, 9)
    last_repair_date = generate_rand_date(datetime.date(2020, 6, 1), datetime.date(2021, 6, 1))
    engine_power = random.randint(100, 200)
    air_conditioner = generate_rand_bool(80)
    transmission = random.choice(transmission_types)
    fuel_cons = round(random.uniform(6.5, 12), 2)
    prod_year = random.randint(1995, 2015)
    drive_type = random.choice(drive_types)
    mileage = random.randint(175, 275)
    engine_type = random.choice(engine_types)
    trunk_volume = random.randint(200, 600)
    color = random.choice(colors)
    steering_side = generate_steering_side()
    min_driver_age = random.randint(18, 21)
    rating = round(random.uniform(3, 5), 2)

    sql_string = "INSERT INTO cars (number, modelID, orderID, placeID, photo, car_type, cost_per_day, max_passenger_number," \
                 "last_repair_date, engine_power, air_conditioner, transmission, fuel_cons, prod_year, drive_type, mileage," \
                 "engine_type, trunk_volume, color, steering_side, min_driver_age, rating) " \
                 f"VALUES ('{number}', {model_id}, {order_id}, {place_id}, '{photo}', '{car_type}', {cost_per_day}, {max_passenger_number}," \
                 f"'{last_repair_date}', {engine_power}, {air_conditioner}, '{transmission}', {fuel_cons}, {prod_year}, '{drive_type}', {mileage}," \
                 f"'{engine_type}', {trunk_volume}, '{color}', '{steering_side}', {min_driver_age}, {rating});"

    f.write(sql_string + '\n')

f.close()
