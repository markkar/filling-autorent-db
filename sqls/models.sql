insert into models (id, name, brand)
values (1, 'Elise', 'Lotus');
insert into models (id, name, brand)
values (2, 'Rocky', 'Daihatsu');
insert into models (id, name, brand)
values (3, 'SLS-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (4, '6000', 'Pontiac');
insert into models (id, name, brand)
values (5, 'LS', 'Lexus');
insert into models (id, name, brand)
values (6, 'SC', 'Lexus');
insert into models (id, name, brand)
values (7, 'GLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (8, 'Lumina', 'Chevrolet');
insert into models (id, name, brand)
values (9, 'Cobalt', 'Chevrolet');
insert into models (id, name, brand)
values (10, 'Rio', 'Kia');
insert into models (id, name, brand)
values (11, 'Yukon XL 2500', 'GMC');
insert into models (id, name, brand)
values (12, 'Caprice', 'Chevrolet');
insert into models (id, name, brand)
values (13, 'GLC', 'Mazda');
insert into models (id, name, brand)
values (14, 'Odyssey', 'Honda');
insert into models (id, name, brand)
values (15, 'B-Series', 'Mazda');
insert into models (id, name, brand)
values (16, 'Explorer Sport Trac', 'Ford');
insert into models (id, name, brand)
values (17, 'Explorer Sport Trac', 'Ford');
insert into models (id, name, brand)
values (18, 'Crown Victoria', 'Ford');
insert into models (id, name, brand)
values (19, 'Discovery', 'Land Rover');
insert into models (id, name, brand)
values (20, 'Daytona', 'Dodge');
insert into models (id, name, brand)
values (21, 'S40', 'Volvo');
insert into models (id, name, brand)
values (22, 'xA', 'Scion');
insert into models (id, name, brand)
values (23, 'Savana 3500', 'GMC');
insert into models (id, name, brand)
values (24, 'Suburban 1500', 'Chevrolet');
insert into models (id, name, brand)
values (25, 'D350 Club', 'Dodge');
insert into models (id, name, brand)
values (26, 'Falcon', 'Ford');
insert into models (id, name, brand)
values (27, 'E-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (28, 'Neon', 'Dodge');
insert into models (id, name, brand)
values (29, 'xB', 'Scion');
insert into models (id, name, brand)
values (30, 'Fairlane', 'Ford');
insert into models (id, name, brand)
values (31, 'Land Cruiser', 'Toyota');
insert into models (id, name, brand)
values (32, 'Sebring', 'Chrysler');
insert into models (id, name, brand)
values (33, 'S6', 'Audi');
insert into models (id, name, brand)
values (34, 'S-Type', 'Jaguar');
insert into models (id, name, brand)
values (35, 'Grand Cherokee', 'Jeep');
insert into models (id, name, brand)
values (36, 'Grand Caravan', 'Dodge');
insert into models (id, name, brand)
values (37, 'Cayman', 'Porsche');
insert into models (id, name, brand)
values (38, 'Neon', 'Dodge');
insert into models (id, name, brand)
values (39, 'XG350', 'Hyundai');
insert into models (id, name, brand)
values (40, '645', 'BMW');
insert into models (id, name, brand)
values (41, 'Altima', 'Nissan');
insert into models (id, name, brand)
values (42, 'Element', 'Honda');
insert into models (id, name, brand)
values (43, 'Canyon', 'GMC');
insert into models (id, name, brand)
values (44, 'S-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (45, '330', 'BMW');
insert into models (id, name, brand)
values (46, '88', 'Oldsmobile');
insert into models (id, name, brand)
values (47, 'W201', 'Mercedes-Benz');
insert into models (id, name, brand)
values (48, 'A6', 'Audi');
insert into models (id, name, brand)
values (49, 'Outlander Sport', 'Mitsubishi');
insert into models (id, name, brand)
values (50, 'F-Series', 'Ford');
insert into models (id, name, brand)
values (51, 'GTO', 'Pontiac');
insert into models (id, name, brand)
values (52, 'Boxster', 'Porsche');
insert into models (id, name, brand)
values (53, 'Odyssey', 'Honda');
insert into models (id, name, brand)
values (54, 'S80', 'Volvo');
insert into models (id, name, brand)
values (55, '190E', 'Mercedes-Benz');
insert into models (id, name, brand)
values (56, 'Caliber', 'Dodge');
insert into models (id, name, brand)
values (57, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (58, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (59, 'Express 1500', 'Chevrolet');
insert into models (id, name, brand)
values (60, 'Escape', 'Ford');
insert into models (id, name, brand)
values (61, '1500', 'GMC');
insert into models (id, name, brand)
values (62, '3500', 'GMC');
insert into models (id, name, brand)
values (63, 'Cobalt', 'Chevrolet');
insert into models (id, name, brand)
values (64, 'Concorde', 'Chrysler');
insert into models (id, name, brand)
values (65, 'Park Avenue', 'Buick');
insert into models (id, name, brand)
values (66, 'S8', 'Audi');
insert into models (id, name, brand)
values (67, 'Gemini', 'Pontiac');
insert into models (id, name, brand)
values (68, 'Concorde', 'Chrysler');
insert into models (id, name, brand)
values (69, '300E', 'Mercedes-Benz');
insert into models (id, name, brand)
values (70, 'Sundance', 'Plymouth');
insert into models (id, name, brand)
values (71, 'Bravada', 'Oldsmobile');
insert into models (id, name, brand)
values (72, 'Yukon XL 2500', 'GMC');
insert into models (id, name, brand)
values (73, 'Miata MX-5', 'Mazda');
insert into models (id, name, brand)
values (74, 'Montero', 'Mitsubishi');
insert into models (id, name, brand)
values (75, 'Grand Vitara', 'Suzuki');
insert into models (id, name, brand)
values (76, 'S4', 'Audi');
insert into models (id, name, brand)
values (77, 'X3', 'BMW');
insert into models (id, name, brand)
values (78, 'Z4', 'BMW');
insert into models (id, name, brand)
values (79, 'Eclipse', 'Mitsubishi');
insert into models (id, name, brand)
values (80, 'Suburban 2500', 'Chevrolet');
insert into models (id, name, brand)
values (81, 'Diablo', 'Lamborghini');
insert into models (id, name, brand)
values (82, 'Rodeo', 'Isuzu');
insert into models (id, name, brand)
values (83, 'Equinox', 'Chevrolet');
insert into models (id, name, brand)
values (84, 'CR-V', 'Honda');
insert into models (id, name, brand)
values (85, 'F150', 'Ford');
insert into models (id, name, brand)
values (86, 'S6', 'Audi');
insert into models (id, name, brand)
values (87, 'GX', 'Lexus');
insert into models (id, name, brand)
values (88, 'A8', 'Audi');
insert into models (id, name, brand)
values (89, 'E-Series', 'Ford');
insert into models (id, name, brand)
values (90, 'GTO', 'Mitsubishi');
insert into models (id, name, brand)
values (91, 'Grand Am', 'Pontiac');
insert into models (id, name, brand)
values (92, 'MPV', 'Mazda');
insert into models (id, name, brand)
values (93, 'Crown Victoria', 'Ford');
insert into models (id, name, brand)
values (94, 'Compass', 'Jeep');
insert into models (id, name, brand)
values (95, 'Exige', 'Lotus');
insert into models (id, name, brand)
values (96, 'Jimmy', 'GMC');
insert into models (id, name, brand)
values (97, 'RAV4', 'Toyota');
insert into models (id, name, brand)
values (98, 'Biturbo', 'Maserati');
insert into models (id, name, brand)
values (99, 'Sentra', 'Nissan');
insert into models (id, name, brand)
values (100, 'Quest', 'Nissan');
insert into models (id, name, brand)
values (101, 'Cougar', 'Mercury');
insert into models (id, name, brand)
values (102, 'LeBaron', 'Chrysler');
insert into models (id, name, brand)
values (103, 'Rally Wagon 3500', 'GMC');
insert into models (id, name, brand)
values (104, 'Cirrus', 'Chrysler');
insert into models (id, name, brand)
values (105, 'Coachbuilder', 'Buick');
insert into models (id, name, brand)
values (106, '57S', 'Maybach');
insert into models (id, name, brand)
values (107, 'Suburban 2500', 'GMC');
insert into models (id, name, brand)
values (108, 'Sierra 3500', 'GMC');
insert into models (id, name, brand)
values (109, 'S80', 'Volvo');
insert into models (id, name, brand)
values (110, 'SL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (111, 'Ram 1500', 'Dodge');
insert into models (id, name, brand)
values (112, 'Riviera', 'Buick');
insert into models (id, name, brand)
values (113, 'Mirage', 'Mitsubishi');
insert into models (id, name, brand)
values (114, 'Elantra', 'Hyundai');
insert into models (id, name, brand)
values (115, 'Ram 1500', 'Dodge');
insert into models (id, name, brand)
values (116, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (117, 'Sienna', 'Toyota');
insert into models (id, name, brand)
values (118, 'G', 'Infiniti');
insert into models (id, name, brand)
values (119, 'Savana 3500', 'GMC');
insert into models (id, name, brand)
values (120, 'Charger', 'Dodge');
insert into models (id, name, brand)
values (121, 'Crossfire Roadster', 'Chrysler');
insert into models (id, name, brand)
values (122, 'Impala', 'Chevrolet');
insert into models (id, name, brand)
values (123, 'LeMans', 'Pontiac');
insert into models (id, name, brand)
values (124, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (125, 'Jetta III', 'Volkswagen');
insert into models (id, name, brand)
values (126, 'Breeze', 'Plymouth');
insert into models (id, name, brand)
values (127, 'Insight', 'Honda');
insert into models (id, name, brand)
values (128, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (129, 'Eurovan', 'Volkswagen');
insert into models (id, name, brand)
values (130, 'Galant', 'Mitsubishi');
insert into models (id, name, brand)
values (131, 'A6', 'Audi');
insert into models (id, name, brand)
values (132, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (133, 'F150', 'Ford');
insert into models (id, name, brand)
values (134, 'S4', 'Audi');
insert into models (id, name, brand)
values (135, 'Colt Vista', 'Plymouth');
insert into models (id, name, brand)
values (136, 'Excel', 'Hyundai');
insert into models (id, name, brand)
values (137, 'Escalade', 'Cadillac');
insert into models (id, name, brand)
values (138, 'A4', 'Audi');
insert into models (id, name, brand)
values (139, 'Express 1500', 'Chevrolet');
insert into models (id, name, brand)
values (140, 'Vitara', 'Suzuki');
insert into models (id, name, brand)
values (141, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (142, 'Mazda3', 'Mazda');
insert into models (id, name, brand)
values (143, '4Runner', 'Toyota');
insert into models (id, name, brand)
values (144, 'Maxima', 'Nissan');
insert into models (id, name, brand)
values (145, 'Roadster', 'Tesla');
insert into models (id, name, brand)
values (146, 'G37', 'Infiniti');
insert into models (id, name, brand)
values (147, 'A6', 'Audi');
insert into models (id, name, brand)
values (148, 'LS', 'Lexus');
insert into models (id, name, brand)
values (149, 'Highlander', 'Toyota');
insert into models (id, name, brand)
values (150, 'Explorer Sport', 'Ford');
insert into models (id, name, brand)
values (151, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (152, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (153, 'Odyssey', 'Honda');
insert into models (id, name, brand)
values (154, 'Skyhawk', 'Buick');
insert into models (id, name, brand)
values (155, 'Cherokee', 'Jeep');
insert into models (id, name, brand)
values (156, '3 Series', 'BMW');
insert into models (id, name, brand)
values (157, 'xB', 'Scion');
insert into models (id, name, brand)
values (158, 'Savana 1500', 'GMC');
insert into models (id, name, brand)
values (159, 'RX', 'Lexus');
insert into models (id, name, brand)
values (160, 'Tracker', 'Chevrolet');
insert into models (id, name, brand)
values (161, 'DBS', 'Aston Martin');
insert into models (id, name, brand)
values (162, 'E-Series', 'Ford');
insert into models (id, name, brand)
values (163, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (164, 'DB9', 'Aston Martin');
insert into models (id, name, brand)
values (165, '6000', 'Pontiac');
insert into models (id, name, brand)
values (166, 'Golf III', 'Volkswagen');
insert into models (id, name, brand)
values (167, 'LS', 'Lexus');
insert into models (id, name, brand)
values (168, 'Expedition', 'Ford');
insert into models (id, name, brand)
values (169, 'Diamante', 'Mitsubishi');
insert into models (id, name, brand)
values (170, 'Accent', 'Hyundai');
insert into models (id, name, brand)
values (171, 'Camaro', 'Chevrolet');
insert into models (id, name, brand)
values (172, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (173, 'Tiburon', 'Hyundai');
insert into models (id, name, brand)
values (174, 'Charger', 'Dodge');
insert into models (id, name, brand)
values (175, 'Skylark', 'Buick');
insert into models (id, name, brand)
values (176, 'Spyder', 'Maserati');
insert into models (id, name, brand)
values (177, 'SRX', 'Cadillac');
insert into models (id, name, brand)
values (178, 'GTO', 'Mitsubishi');
insert into models (id, name, brand)
values (179, 'S60', 'Volvo');
insert into models (id, name, brand)
values (180, 'Skyhawk', 'Buick');
insert into models (id, name, brand)
values (181, 'Explorer', 'Ford');
insert into models (id, name, brand)
values (182, 'Esteem', 'Suzuki');
insert into models (id, name, brand)
values (183, 'S10', 'Chevrolet');
insert into models (id, name, brand)
values (184, 'E150', 'Ford');
insert into models (id, name, brand)
values (185, 'Pajero', 'Mitsubishi');
insert into models (id, name, brand)
values (186, 'Sportvan G20', 'Chevrolet');
insert into models (id, name, brand)
values (187, 'Talon', 'Eagle');
insert into models (id, name, brand)
values (188, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (189, 'F350', 'Ford');
insert into models (id, name, brand)
values (190, 'Datsun/Nissan Z-car', 'Nissan');
insert into models (id, name, brand)
values (191, 'Impreza', 'Subaru');
insert into models (id, name, brand)
values (192, 'M3', 'BMW');
insert into models (id, name, brand)
values (193, 'Escalade ESV', 'Cadillac');
insert into models (id, name, brand)
values (194, 'Prowler', 'Chrysler');
insert into models (id, name, brand)
values (195, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (196, 'Discovery Series II', 'Land Rover');
insert into models (id, name, brand)
values (197, 'E-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (198, 'M5', 'BMW');
insert into models (id, name, brand)
values (199, 'Venza', 'Toyota');
insert into models (id, name, brand)
values (200, 'Topaz', 'Mercury');
insert into models (id, name, brand)
values (201, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (202, 'Celica', 'Toyota');
insert into models (id, name, brand)
values (203, 'Mark VIII', 'Lincoln');
insert into models (id, name, brand)
values (204, 'Journey', 'Dodge');
insert into models (id, name, brand)
values (205, 'GranSport', 'Maserati');
insert into models (id, name, brand)
values (206, 'Charger', 'Dodge');
insert into models (id, name, brand)
values (207, 'Continental GT', 'Bentley');
insert into models (id, name, brand)
values (208, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (209, 'X6 M', 'BMW');
insert into models (id, name, brand)
values (210, 'Regal', 'Buick');
insert into models (id, name, brand)
values (211, 'RL', 'Acura');
insert into models (id, name, brand)
values (212, 'Rio', 'Kia');
insert into models (id, name, brand)
values (213, 'Venture', 'Chevrolet');
insert into models (id, name, brand)
values (214, 'PT Cruiser', 'Chrysler');
insert into models (id, name, brand)
values (215, 'Ram Wagon B250', 'Dodge');
insert into models (id, name, brand)
values (216, 'Camry', 'Toyota');
insert into models (id, name, brand)
values (217, 'Ram 2500 Club', 'Dodge');
insert into models (id, name, brand)
values (218, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (219, 'Frontier', 'Nissan');
insert into models (id, name, brand)
values (220, 'MDX', 'Acura');
insert into models (id, name, brand)
values (221, 'MPV', 'Mazda');
insert into models (id, name, brand)
values (222, '3 Series', 'BMW');
insert into models (id, name, brand)
values (223, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (224, '2500', 'Chevrolet');
insert into models (id, name, brand)
values (225, 'Esperante', 'Panoz');
insert into models (id, name, brand)
values (226, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (227, 'Ram Van B150', 'Dodge');
insert into models (id, name, brand)
values (228, 'Expedition EL', 'Ford');
insert into models (id, name, brand)
values (229, 'Canyon', 'GMC');
insert into models (id, name, brand)
values (230, 'B-Series', 'Mazda');
insert into models (id, name, brand)
values (231, 'Forester', 'Subaru');
insert into models (id, name, brand)
values (232, 'XK', 'Jaguar');
insert into models (id, name, brand)
values (233, 'Daytona', 'Dodge');
insert into models (id, name, brand)
values (234, 'NSX', 'Acura');
insert into models (id, name, brand)
values (235, 'Land Cruiser', 'Toyota');
insert into models (id, name, brand)
values (236, 'Astro', 'Chevrolet');
insert into models (id, name, brand)
values (237, 'Jimmy', 'GMC');
insert into models (id, name, brand)
values (238, 'Amigo', 'Isuzu');
insert into models (id, name, brand)
values (239, 'S-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (240, 'Loyale', 'Subaru');
insert into models (id, name, brand)
values (241, 'Town & Country', 'Chrysler');
insert into models (id, name, brand)
values (242, '745', 'BMW');
insert into models (id, name, brand)
values (243, 'Alero', 'Oldsmobile');
insert into models (id, name, brand)
values (244, 'Gallardo', 'Lamborghini');
insert into models (id, name, brand)
values (245, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (246, 'Edge', 'Ford');
insert into models (id, name, brand)
values (247, 'Grand Cherokee', 'Jeep');
insert into models (id, name, brand)
values (248, 'Voyager', 'Plymouth');
insert into models (id, name, brand)
values (249, 'S2000', 'Honda');
insert into models (id, name, brand)
values (250, 'Armada', 'Nissan');
insert into models (id, name, brand)
values (251, 'Civic GX', 'Honda');
insert into models (id, name, brand)
values (252, '88', 'Oldsmobile');
insert into models (id, name, brand)
values (253, 'Forester', 'Subaru');
insert into models (id, name, brand)
values (254, 'Mentor', 'Kia');
insert into models (id, name, brand)
values (255, 'Dakota Club', 'Dodge');
insert into models (id, name, brand)
values (256, 'Park Avenue', 'Buick');
insert into models (id, name, brand)
values (257, 'Frontier', 'Nissan');
insert into models (id, name, brand)
values (258, 'Eclipse', 'Mitsubishi');
insert into models (id, name, brand)
values (259, 'Corvette', 'Chevrolet');
insert into models (id, name, brand)
values (260, 'Grand Caravan', 'Dodge');
insert into models (id, name, brand)
values (261, 'F250', 'Ford');
insert into models (id, name, brand)
values (262, 'CTS', 'Cadillac');
insert into models (id, name, brand)
values (263, 'Regal', 'Buick');
insert into models (id, name, brand)
values (264, 'Brat', 'Subaru');
insert into models (id, name, brand)
values (265, 'Crown Victoria', 'Ford');
insert into models (id, name, brand)
values (266, 'SVX', 'Subaru');
insert into models (id, name, brand)
values (267, 'Accord', 'Honda');
insert into models (id, name, brand)
values (268, 'NSX', 'Acura');
insert into models (id, name, brand)
values (269, 'X6 M', 'BMW');
insert into models (id, name, brand)
values (270, 'Quattro', 'Audi');
insert into models (id, name, brand)
values (271, 'SLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (272, 'Excursion', 'Ford');
insert into models (id, name, brand)
values (273, 'DBS', 'Aston Martin');
insert into models (id, name, brand)
values (274, 'MKS', 'Lincoln');
insert into models (id, name, brand)
values (275, 'Ram 1500', 'Dodge');
insert into models (id, name, brand)
values (276, '9-5', 'Saab');
insert into models (id, name, brand)
values (277, 'CR-V', 'Honda');
insert into models (id, name, brand)
values (278, 'Sonoma', 'GMC');
insert into models (id, name, brand)
values (279, 'ES', 'Lexus');
insert into models (id, name, brand)
values (280, 'RAV4', 'Toyota');
insert into models (id, name, brand)
values (281, 'Legend', 'Acura');
insert into models (id, name, brand)
values (282, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (283, 'Trooper', 'Isuzu');
insert into models (id, name, brand)
values (284, '80', 'Audi');
insert into models (id, name, brand)
values (285, 'M5', 'BMW');
insert into models (id, name, brand)
values (286, 'Murano', 'Nissan');
insert into models (id, name, brand)
values (287, 'DTS', 'Cadillac');
insert into models (id, name, brand)
values (288, 'TSX', 'Acura');
insert into models (id, name, brand)
values (289, 'Lumina', 'Chevrolet');
insert into models (id, name, brand)
values (290, 'Savana', 'GMC');
insert into models (id, name, brand)
values (291, 'Leone', 'Subaru');
insert into models (id, name, brand)
values (292, 'Prius', 'Toyota');
insert into models (id, name, brand)
values (293, 'Achieva', 'Oldsmobile');
insert into models (id, name, brand)
values (294, 'Prizm', 'Geo');
insert into models (id, name, brand)
values (295, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (296, 'Avalanche 1500', 'Chevrolet');
insert into models (id, name, brand)
values (297, 'Outlander', 'Mitsubishi');
insert into models (id, name, brand)
values (298, 'Protege', 'Mazda');
insert into models (id, name, brand)
values (299, 'Fox', 'Volkswagen');
insert into models (id, name, brand)
values (300, 'Optima', 'Kia');
insert into models (id, name, brand)
values (301, 'QX', 'Infiniti');
insert into models (id, name, brand)
values (302, 'Santa Fe', 'Hyundai');
insert into models (id, name, brand)
values (303, 'i-370', 'Isuzu');
insert into models (id, name, brand)
values (304, 'FX', 'Infiniti');
insert into models (id, name, brand)
values (305, '9-4X', 'Saab');
insert into models (id, name, brand)
values (306, 'Legacy', 'Subaru');
insert into models (id, name, brand)
values (307, 'Ram', 'Dodge');
insert into models (id, name, brand)
values (308, 'Cirrus', 'Chrysler');
insert into models (id, name, brand)
values (309, 'Sigma', 'Mitsubishi');
insert into models (id, name, brand)
values (310, 'Esprit', 'Lotus');
insert into models (id, name, brand)
values (311, 'HHR', 'Chevrolet');
insert into models (id, name, brand)
values (312, 'Eos', 'Volkswagen');
insert into models (id, name, brand)
values (313, 'Mazdaspeed 3', 'Mazda');
insert into models (id, name, brand)
values (314, 'X5', 'BMW');
insert into models (id, name, brand)
values (315, 'NSX', 'Acura');
insert into models (id, name, brand)
values (316, 'Rondo', 'Kia');
insert into models (id, name, brand)
values (317, 'A8', 'Audi');
insert into models (id, name, brand)
values (318, 'X6 M', 'BMW');
insert into models (id, name, brand)
values (319, '3500', 'GMC');
insert into models (id, name, brand)
values (320, 'Tempo', 'Ford');
insert into models (id, name, brand)
values (321, 'Scirocco', 'Volkswagen');
insert into models (id, name, brand)
values (322, 'Odyssey', 'Honda');
insert into models (id, name, brand)
values (323, 'Mountaineer', 'Mercury');
insert into models (id, name, brand)
values (324, '3 Series', 'BMW');
insert into models (id, name, brand)
values (325, 'XL-7', 'Suzuki');
insert into models (id, name, brand)
values (326, '430', 'Maserati');
insert into models (id, name, brand)
values (327, 'MR2', 'Toyota');
insert into models (id, name, brand)
values (328, '90', 'Audi');
insert into models (id, name, brand)
values (329, '9-2X', 'Saab');
insert into models (id, name, brand)
values (330, 'Monte Carlo', 'Chevrolet');
insert into models (id, name, brand)
values (331, 'Accent', 'Hyundai');
insert into models (id, name, brand)
values (332, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (333, 'Scirocco', 'Volkswagen');
insert into models (id, name, brand)
values (334, 'GX', 'Lexus');
insert into models (id, name, brand)
values (335, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (336, 'Corvette', 'Chevrolet');
insert into models (id, name, brand)
values (337, 'SJ', 'Suzuki');
insert into models (id, name, brand)
values (338, 'Celica', 'Toyota');
insert into models (id, name, brand)
values (339, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (340, 'Topaz', 'Mercury');
insert into models (id, name, brand)
values (341, 'Sentra', 'Nissan');
insert into models (id, name, brand)
values (342, 'Highlander', 'Toyota');
insert into models (id, name, brand)
values (343, 'Miata MX-5', 'Mazda');
insert into models (id, name, brand)
values (344, 'Avenger', 'Dodge');
insert into models (id, name, brand)
values (345, 'ZDX', 'Acura');
insert into models (id, name, brand)
values (346, 'Montero', 'Mitsubishi');
insert into models (id, name, brand)
values (347, 'Type 2', 'Volkswagen');
insert into models (id, name, brand)
values (348, '240SX', 'Nissan');
insert into models (id, name, brand)
values (349, 'Elantra', 'Hyundai');
insert into models (id, name, brand)
values (350, 'Outlander Sport', 'Mitsubishi');
insert into models (id, name, brand)
values (351, 'Land Cruiser', 'Toyota');
insert into models (id, name, brand)
values (352, 'Sienna', 'Toyota');
insert into models (id, name, brand)
values (353, 'Regal', 'Buick');
insert into models (id, name, brand)
values (354, 'E250', 'Ford');
insert into models (id, name, brand)
values (355, 'CC', 'Volkswagen');
insert into models (id, name, brand)
values (356, 'Accord Crosstour', 'Honda');
insert into models (id, name, brand)
values (357, 'Optima', 'Kia');
insert into models (id, name, brand)
values (358, 'Land Cruiser', 'Toyota');
insert into models (id, name, brand)
values (359, 'GTI', 'Volkswagen');
insert into models (id, name, brand)
values (360, 'Escalade', 'Cadillac');
insert into models (id, name, brand)
values (361, 'Bel Air', 'Chevrolet');
insert into models (id, name, brand)
values (362, 'Escalade EXT', 'Cadillac');
insert into models (id, name, brand)
values (363, 'Electra', 'Buick');
insert into models (id, name, brand)
values (364, 'Phantom', 'Rolls-Royce');
insert into models (id, name, brand)
values (365, 'Sequoia', 'Toyota');
insert into models (id, name, brand)
values (366, 'C-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (367, 'Gemini', 'Pontiac');
insert into models (id, name, brand)
values (368, '911', 'Porsche');
insert into models (id, name, brand)
values (369, 'Explorer Sport Trac', 'Ford');
insert into models (id, name, brand)
values (370, 'S4', 'Audi');
insert into models (id, name, brand)
values (371, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (372, 'Maxima', 'Nissan');
insert into models (id, name, brand)
values (373, 'Golf', 'Volkswagen');
insert into models (id, name, brand)
values (374, '300ZX', 'Nissan');
insert into models (id, name, brand)
values (375, 'Vibe', 'Pontiac');
insert into models (id, name, brand)
values (376, 'S80', 'Volvo');
insert into models (id, name, brand)
values (377, 'Millenia', 'Mazda');
insert into models (id, name, brand)
values (378, 'SLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (379, 'Mirage', 'Mitsubishi');
insert into models (id, name, brand)
values (380, 'RS4', 'Audi');
insert into models (id, name, brand)
values (381, 'Suburban 1500', 'GMC');
insert into models (id, name, brand)
values (382, 'M5', 'BMW');
insert into models (id, name, brand)
values (383, '300SD', 'Mercedes-Benz');
insert into models (id, name, brand)
values (384, 'Accord', 'Honda');
insert into models (id, name, brand)
values (385, 'Sephia', 'Kia');
insert into models (id, name, brand)
values (386, '1500 Club Coupe', 'GMC');
insert into models (id, name, brand)
values (387, 'CLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (388, 'Metro', 'Geo');
insert into models (id, name, brand)
values (389, 'Savana 2500', 'GMC');
insert into models (id, name, brand)
values (390, 'Mini Cooper', 'Austin');
insert into models (id, name, brand)
values (391, 'Familia', 'Mazda');
insert into models (id, name, brand)
values (392, 'New Yorker', 'Chrysler');
insert into models (id, name, brand)
values (393, 'Xterra', 'Nissan');
insert into models (id, name, brand)
values (394, 'Integra', 'Acura');
insert into models (id, name, brand)
values (395, 'TSX', 'Acura');
insert into models (id, name, brand)
values (396, 'FCX Clarity', 'Honda');
insert into models (id, name, brand)
values (397, 'rio', 'Volkswagen');
insert into models (id, name, brand)
values (398, 'Fiero', 'Pontiac');
insert into models (id, name, brand)
values (399, 'XC60', 'Volvo');
insert into models (id, name, brand)
values (400, 'Legend', 'Acura');
insert into models (id, name, brand)
values (401, 'LeSabre', 'Buick');
insert into models (id, name, brand)
values (402, 'Scirocco', 'Volkswagen');
insert into models (id, name, brand)
values (403, 'G6', 'Pontiac');
insert into models (id, name, brand)
values (404, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (405, 'Savana 1500', 'GMC');
insert into models (id, name, brand)
values (406, 'Fortwo', 'Smart');
insert into models (id, name, brand)
values (407, 'Mazda6', 'Mazda');
insert into models (id, name, brand)
values (408, 'RX', 'Lexus');
insert into models (id, name, brand)
values (409, 'Frontier', 'Nissan');
insert into models (id, name, brand)
values (410, 'Mighty Max', 'Mitsubishi');
insert into models (id, name, brand)
values (411, 'LS', 'Lexus');
insert into models (id, name, brand)
values (412, 'Grand Cherokee', 'Jeep');
insert into models (id, name, brand)
values (413, 'Cayenne', 'Porsche');
insert into models (id, name, brand)
values (414, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (415, 'Sierra 2500', 'GMC');
insert into models (id, name, brand)
values (416, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (417, 'Dakota Club', 'Dodge');
insert into models (id, name, brand)
values (418, 'Club Wagon', 'Ford');
insert into models (id, name, brand)
values (419, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (420, 'Xterra', 'Nissan');
insert into models (id, name, brand)
values (421, 'Cayman', 'Porsche');
insert into models (id, name, brand)
values (422, 'Echo', 'Toyota');
insert into models (id, name, brand)
values (423, 'RX', 'Lexus');
insert into models (id, name, brand)
values (424, 'SJ 410', 'Suzuki');
insert into models (id, name, brand)
values (425, 'S-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (426, 'Expedition', 'Ford');
insert into models (id, name, brand)
values (427, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (428, 'Trooper', 'Isuzu');
insert into models (id, name, brand)
values (429, 'Bronco', 'Ford');
insert into models (id, name, brand)
values (430, 'XK Series', 'Jaguar');
insert into models (id, name, brand)
values (431, '5000CS', 'Audi');
insert into models (id, name, brand)
values (432, 'CR-V', 'Honda');
insert into models (id, name, brand)
values (433, 'Rally Wagon G3500', 'GMC');
insert into models (id, name, brand)
values (434, 'Quattroporte', 'Maserati');
insert into models (id, name, brand)
values (435, 'Defender 110', 'Land Rover');
insert into models (id, name, brand)
values (436, 'Cavalier', 'Chevrolet');
insert into models (id, name, brand)
values (437, 'Accord', 'Honda');
insert into models (id, name, brand)
values (438, 'Express', 'Chevrolet');
insert into models (id, name, brand)
values (439, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (440, '7 Series', 'BMW');
insert into models (id, name, brand)
values (441, 'RVR', 'Mitsubishi');
insert into models (id, name, brand)
values (442, 'SL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (443, 'GTI', 'Volkswagen');
insert into models (id, name, brand)
values (444, 'Firebird', 'Pontiac');
insert into models (id, name, brand)
values (445, 'X6 M', 'BMW');
insert into models (id, name, brand)
values (446, 'Cabriolet', 'Volkswagen');
insert into models (id, name, brand)
values (447, 'ZX2', 'Ford');
insert into models (id, name, brand)
values (448, 'Insight', 'Honda');
insert into models (id, name, brand)
values (449, 'Cruze', 'Chevrolet');
insert into models (id, name, brand)
values (450, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (451, 'Mazda6', 'Mazda');
insert into models (id, name, brand)
values (452, 'New Yorker', 'Chrysler');
insert into models (id, name, brand)
values (453, 'Econoline E350', 'Ford');
insert into models (id, name, brand)
values (454, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (455, 'Pilot', 'Honda');
insert into models (id, name, brand)
values (456, 'Express 3500', 'Chevrolet');
insert into models (id, name, brand)
values (457, 'Silverado 3500HD', 'Chevrolet');
insert into models (id, name, brand)
values (458, 'B-Series Plus', 'Mazda');
insert into models (id, name, brand)
values (459, 'F150', 'Ford');
insert into models (id, name, brand)
values (460, 'Sentra', 'Nissan');
insert into models (id, name, brand)
values (461, 'Impala', 'Chevrolet');
insert into models (id, name, brand)
values (462, 'Aveo', 'Chevrolet');
insert into models (id, name, brand)
values (463, 'Silverado 2500', 'Chevrolet');
insert into models (id, name, brand)
values (464, 'CL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (465, '4Runner', 'Toyota');
insert into models (id, name, brand)
values (466, 'Miata MX-5', 'Mazda');
insert into models (id, name, brand)
values (467, 'G-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (468, 'A4', 'Audi');
insert into models (id, name, brand)
values (469, 'Cooper Countryman', 'MINI');
insert into models (id, name, brand)
values (470, 'Sedona', 'Kia');
insert into models (id, name, brand)
values (471, 'Skyhawk', 'Buick');
insert into models (id, name, brand)
values (472, '645', 'BMW');
insert into models (id, name, brand)
values (473, 'Freelander', 'Land Rover');
insert into models (id, name, brand)
values (474, 'GS', 'Lexus');
insert into models (id, name, brand)
values (475, 'G-Series 3500', 'Chevrolet');
insert into models (id, name, brand)
values (476, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (477, 'RX', 'Lexus');
insert into models (id, name, brand)
values (478, 'C-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (479, 'Sparrow', 'Corbin');
insert into models (id, name, brand)
values (480, 'SL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (481, 'Silverado 1500', 'Chevrolet');
insert into models (id, name, brand)
values (482, 'Soul', 'Kia');
insert into models (id, name, brand)
values (483, 'Venture', 'Chevrolet');
insert into models (id, name, brand)
values (484, 'Silverado 2500', 'Chevrolet');
insert into models (id, name, brand)
values (485, 'Spectra', 'Kia');
insert into models (id, name, brand)
values (486, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (487, 'Ram Van 3500', 'Dodge');
insert into models (id, name, brand)
values (488, 'GX', 'Lexus');
insert into models (id, name, brand)
values (489, 'Maxima', 'Nissan');
insert into models (id, name, brand)
values (490, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (491, 'Mazda6 Sport', 'Mazda');
insert into models (id, name, brand)
values (492, 'Sonata', 'Hyundai');
insert into models (id, name, brand)
values (493, 'Loyale', 'Subaru');
insert into models (id, name, brand)
values (494, '6 Series', 'BMW');
insert into models (id, name, brand)
values (495, 'S6', 'Audi');
insert into models (id, name, brand)
values (496, 'Focus', 'Ford');
insert into models (id, name, brand)
values (497, 'Prelude', 'Honda');
insert into models (id, name, brand)
values (498, 'Thunderbird', 'Ford');
insert into models (id, name, brand)
values (499, 'F-Series', 'Ford');
insert into models (id, name, brand)
values (500, '4000s', 'Audi');
insert into models (id, name, brand)
values (501, 'Chariot', 'Mitsubishi');
insert into models (id, name, brand)
values (502, 'Legacy', 'Subaru');
insert into models (id, name, brand)
values (503, 'Continental Flying Spur', 'Bentley');
insert into models (id, name, brand)
values (504, 'Gran Sport', 'Maserati');
insert into models (id, name, brand)
values (505, 'Focus', 'Ford');
insert into models (id, name, brand)
values (506, 'Forester', 'Subaru');
insert into models (id, name, brand)
values (507, 'G5', 'Pontiac');
insert into models (id, name, brand)
values (508, '928', 'Porsche');
insert into models (id, name, brand)
values (509, 'Pajero', 'Mitsubishi');
insert into models (id, name, brand)
values (510, 'Diamante', 'Mitsubishi');
insert into models (id, name, brand)
values (511, 'Econoline E350', 'Ford');
insert into models (id, name, brand)
values (512, 'Pajero', 'Mitsubishi');
insert into models (id, name, brand)
values (513, 'Vantage', 'Aston Martin');
insert into models (id, name, brand)
values (514, 'Express 2500', 'Chevrolet');
insert into models (id, name, brand)
values (515, 'Century', 'Buick');
insert into models (id, name, brand)
values (516, 'B2600', 'Mazda');
insert into models (id, name, brand)
values (517, 'Phantom', 'Rolls-Royce');
insert into models (id, name, brand)
values (518, 'Golf', 'Volkswagen');
insert into models (id, name, brand)
values (519, 'MPV', 'Mazda');
insert into models (id, name, brand)
values (520, 'C-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (521, 'B2600', 'Mazda');
insert into models (id, name, brand)
values (522, 'Skyhawk', 'Buick');
insert into models (id, name, brand)
values (523, '9-5', 'Saab');
insert into models (id, name, brand)
values (524, 'Z8', 'BMW');
insert into models (id, name, brand)
values (525, 'Cruze', 'Chevrolet');
insert into models (id, name, brand)
values (526, 'Firebird', 'Pontiac');
insert into models (id, name, brand)
values (527, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (528, 'Golf', 'Volkswagen');
insert into models (id, name, brand)
values (529, '300SL', 'Mercedes-Benz');
insert into models (id, name, brand)
values (530, 'Escort', 'Ford');
insert into models (id, name, brand)
values (531, 'A5', 'Audi');
insert into models (id, name, brand)
values (532, 'Caravan', 'Dodge');
insert into models (id, name, brand)
values (533, 'Countryman', 'MINI');
insert into models (id, name, brand)
values (534, 'Aspire', 'Ford');
insert into models (id, name, brand)
values (535, 'Millenia', 'Mazda');
insert into models (id, name, brand)
values (536, 'rio', 'Volkswagen');
insert into models (id, name, brand)
values (537, 'Galant', 'Mitsubishi');
insert into models (id, name, brand)
values (538, '62', 'Maybach');
insert into models (id, name, brand)
values (539, 'CL', 'Acura');
insert into models (id, name, brand)
values (540, 'RX Hybrid', 'Lexus');
insert into models (id, name, brand)
values (541, 'Ram 1500', 'Dodge');
insert into models (id, name, brand)
values (542, 'Passat', 'Volkswagen');
insert into models (id, name, brand)
values (543, 'Rio', 'Kia');
insert into models (id, name, brand)
values (544, 'Esteem', 'Suzuki');
insert into models (id, name, brand)
values (545, 'Mazda6', 'Mazda');
insert into models (id, name, brand)
values (546, 'MKX', 'Lincoln');
insert into models (id, name, brand)
values (547, 'Sonata', 'Hyundai');
insert into models (id, name, brand)
values (548, 'Talon', 'Eagle');
insert into models (id, name, brand)
values (549, 'Riviera', 'Buick');
insert into models (id, name, brand)
values (550, 'Xterra', 'Nissan');
insert into models (id, name, brand)
values (551, 'E-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (552, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (553, 'Frontier', 'Nissan');
insert into models (id, name, brand)
values (554, 'Dakota', 'Dodge');
insert into models (id, name, brand)
values (555, 'E150', 'Ford');
insert into models (id, name, brand)
values (556, 'Tundra', 'Toyota');
insert into models (id, name, brand)
values (557, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (558, 'Cruze', 'Chevrolet');
insert into models (id, name, brand)
values (559, 'Jetta', 'Volkswagen');
insert into models (id, name, brand)
values (560, 'Park Avenue', 'Buick');
insert into models (id, name, brand)
values (561, '944', 'Porsche');
insert into models (id, name, brand)
values (562, 'Civic', 'Honda');
insert into models (id, name, brand)
values (563, 'D250 Club', 'Dodge');
insert into models (id, name, brand)
values (564, 'Avenger', 'Dodge');
insert into models (id, name, brand)
values (565, 'Miata MX-5', 'Mazda');
insert into models (id, name, brand)
values (566, 'X6', 'BMW');
insert into models (id, name, brand)
values (567, 'Silverado 2500', 'Chevrolet');
insert into models (id, name, brand)
values (568, 'Leganza', 'Daewoo');
insert into models (id, name, brand)
values (569, 'A6', 'Audi');
insert into models (id, name, brand)
values (570, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (571, 'Vitara', 'Suzuki');
insert into models (id, name, brand)
values (572, 'Mazda6', 'Mazda');
insert into models (id, name, brand)
values (573, 'Esprit', 'Lotus');
insert into models (id, name, brand)
values (574, 'X5', 'BMW');
insert into models (id, name, brand)
values (575, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (576, 'Impala', 'Chevrolet');
insert into models (id, name, brand)
values (577, 'Town & Country', 'Chrysler');
insert into models (id, name, brand)
values (578, 'Range Rover Sport', 'Land Rover');
insert into models (id, name, brand)
values (579, 'Spectra', 'Kia');
insert into models (id, name, brand)
values (580, 'Scirocco', 'Volkswagen');
insert into models (id, name, brand)
values (581, 'Avanti', 'Studebaker');
insert into models (id, name, brand)
values (582, 'Integra', 'Acura');
insert into models (id, name, brand)
values (583, 'Express 2500', 'Chevrolet');
insert into models (id, name, brand)
values (584, 'Murano', 'Nissan');
insert into models (id, name, brand)
values (585, 'Golf', 'Volkswagen');
insert into models (id, name, brand)
values (586, 'S90', 'Volvo');
insert into models (id, name, brand)
values (587, 'Impala', 'Chevrolet');
insert into models (id, name, brand)
values (588, 'Laser', 'Ford');
insert into models (id, name, brand)
values (589, 'Grand Voyager', 'Plymouth');
insert into models (id, name, brand)
values (590, 'Cayenne', 'Porsche');
insert into models (id, name, brand)
values (591, 'Aura', 'Saturn');
insert into models (id, name, brand)
values (592, 'IS', 'Lexus');
insert into models (id, name, brand)
values (593, 'Challenger', 'Mitsubishi');
insert into models (id, name, brand)
values (594, 'Aerio', 'Suzuki');
insert into models (id, name, brand)
values (595, 'Continental', 'Lincoln');
insert into models (id, name, brand)
values (596, 'Alcyone SVX', 'Subaru');
insert into models (id, name, brand)
values (597, 'Corvette', 'Chevrolet');
insert into models (id, name, brand)
values (598, 'Dakota Club', 'Dodge');
insert into models (id, name, brand)
values (599, 'Civic', 'Honda');
insert into models (id, name, brand)
values (600, 'Integra', 'Acura');
insert into models (id, name, brand)
values (601, 'Continental GT', 'Bentley');
insert into models (id, name, brand)
values (602, '940', 'Volvo');
insert into models (id, name, brand)
values (603, '9-5', 'Saab');
insert into models (id, name, brand)
values (604, 'Montero', 'Mitsubishi');
insert into models (id, name, brand)
values (605, 'Maxima', 'Nissan');
insert into models (id, name, brand)
values (606, 'Tucson', 'Hyundai');
insert into models (id, name, brand)
values (607, 'Tacoma Xtra', 'Toyota');
insert into models (id, name, brand)
values (608, '240', 'Volvo');
insert into models (id, name, brand)
values (609, 'Ram Van B150', 'Dodge');
insert into models (id, name, brand)
values (610, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (611, 'GTI', 'Volkswagen');
insert into models (id, name, brand)
values (612, 'Quattroporte', 'Maserati');
insert into models (id, name, brand)
values (613, '5 Series', 'BMW');
insert into models (id, name, brand)
values (614, 'RAV4', 'Toyota');
insert into models (id, name, brand)
values (615, 'Altima', 'Nissan');
insert into models (id, name, brand)
values (616, '960', 'Volvo');
insert into models (id, name, brand)
values (617, 'Grand Caravan', 'Dodge');
insert into models (id, name, brand)
values (618, 'CLS-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (619, 'Mystique', 'Mercury');
insert into models (id, name, brand)
values (620, 'Journey', 'Dodge');
insert into models (id, name, brand)
values (621, 'E-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (622, '300', 'Chrysler');
insert into models (id, name, brand)
values (623, 'SL65 AMG', 'Mercedes-Benz');
insert into models (id, name, brand)
values (624, '500E', 'Mercedes-Benz');
insert into models (id, name, brand)
values (625, 'SVX', 'Subaru');
insert into models (id, name, brand)
values (626, 'SLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (627, 'Tacoma Xtra', 'Toyota');
insert into models (id, name, brand)
values (628, 'MX-5', 'Mazda');
insert into models (id, name, brand)
values (629, '911', 'Porsche');
insert into models (id, name, brand)
values (630, 'F250', 'Ford');
insert into models (id, name, brand)
values (631, '3000GT', 'Mitsubishi');
insert into models (id, name, brand)
values (632, 'Accent', 'Hyundai');
insert into models (id, name, brand)
values (633, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (634, 'Passport', 'Honda');
insert into models (id, name, brand)
values (635, '2500 Club Coupe', 'GMC');
insert into models (id, name, brand)
values (636, 'Colt Vista', 'Plymouth');
insert into models (id, name, brand)
values (637, 'GT-R', 'Nissan');
insert into models (id, name, brand)
values (638, 'Murano', 'Nissan');
insert into models (id, name, brand)
values (639, 'Bonneville', 'Pontiac');
insert into models (id, name, brand)
values (640, 'Durango', 'Dodge');
insert into models (id, name, brand)
values (641, 'XL-7', 'Suzuki');
insert into models (id, name, brand)
values (642, 'Sigma', 'Mitsubishi');
insert into models (id, name, brand)
values (643, 'Town & Country', 'Chrysler');
insert into models (id, name, brand)
values (644, 'Silverado 3500', 'Chevrolet');
insert into models (id, name, brand)
values (645, 'Trooper', 'Isuzu');
insert into models (id, name, brand)
values (646, 'A8', 'Audi');
insert into models (id, name, brand)
values (647, 'Celica', 'Toyota');
insert into models (id, name, brand)
values (648, 'Silverado 1500', 'Chevrolet');
insert into models (id, name, brand)
values (649, 'Corvette', 'Chevrolet');
insert into models (id, name, brand)
values (650, 'Vibe', 'Pontiac');
insert into models (id, name, brand)
values (651, 'CL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (652, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (653, 'Corvette', 'Chevrolet');
insert into models (id, name, brand)
values (654, 'Continental Mark VII', 'Lincoln');
insert into models (id, name, brand)
values (655, 'Bonneville', 'Pontiac');
insert into models (id, name, brand)
values (656, 'Outlander', 'Mitsubishi');
insert into models (id, name, brand)
values (657, 'Routan', 'Volkswagen');
insert into models (id, name, brand)
values (658, 'XJ Series', 'Jaguar');
insert into models (id, name, brand)
values (659, 'Quattroporte', 'Maserati');
insert into models (id, name, brand)
values (660, 'M5', 'BMW');
insert into models (id, name, brand)
values (661, 'Aspire', 'Ford');
insert into models (id, name, brand)
values (662, 'Concorde', 'Chrysler');
insert into models (id, name, brand)
values (663, 'Rally Wagon 3500', 'GMC');
insert into models (id, name, brand)
values (664, 'Town & Country', 'Chrysler');
insert into models (id, name, brand)
values (665, 'Sebring', 'Chrysler');
insert into models (id, name, brand)
values (666, 'Z4', 'BMW');
insert into models (id, name, brand)
values (667, 'Ram 1500', 'Dodge');
insert into models (id, name, brand)
values (668, 'Prizm', 'Geo');
insert into models (id, name, brand)
values (669, 'A5', 'Audi');
insert into models (id, name, brand)
values (670, 'XT', 'Subaru');
insert into models (id, name, brand)
values (671, 'QX', 'Infiniti');
insert into models (id, name, brand)
values (672, 'Astro', 'Chevrolet');
insert into models (id, name, brand)
values (673, 'Regal', 'Buick');
insert into models (id, name, brand)
values (674, 'MKX', 'Lincoln');
insert into models (id, name, brand)
values (675, 'Golf', 'Volkswagen');
insert into models (id, name, brand)
values (676, 'Skylark', 'Buick');
insert into models (id, name, brand)
values (677, 'Tercel', 'Toyota');
insert into models (id, name, brand)
values (678, 'Achieva', 'Oldsmobile');
insert into models (id, name, brand)
values (679, 'Prizm', 'Geo');
insert into models (id, name, brand)
values (680, 'Previa', 'Toyota');
insert into models (id, name, brand)
values (681, 'RX', 'Lexus');
insert into models (id, name, brand)
values (682, 'ES', 'Lexus');
insert into models (id, name, brand)
values (683, 'TT', 'Audi');
insert into models (id, name, brand)
values (684, 'Expedition', 'Ford');
insert into models (id, name, brand)
values (685, 'Navigator', 'Lincoln');
insert into models (id, name, brand)
values (686, 'Titan', 'Nissan');
insert into models (id, name, brand)
values (687, 'Festiva', 'Ford');
insert into models (id, name, brand)
values (688, '323', 'Mazda');
insert into models (id, name, brand)
values (689, 'Dakota', 'Dodge');
insert into models (id, name, brand)
values (690, 'Chariot', 'Mitsubishi');
insert into models (id, name, brand)
values (691, 'Caravan', 'Dodge');
insert into models (id, name, brand)
values (692, 'TL', 'Acura');
insert into models (id, name, brand)
values (693, 'GTO', 'Mitsubishi');
insert into models (id, name, brand)
values (694, 'Savana 2500', 'GMC');
insert into models (id, name, brand)
values (695, 'Navigator', 'Lincoln');
insert into models (id, name, brand)
values (696, 'E250', 'Ford');
insert into models (id, name, brand)
values (697, 'F350', 'Ford');
insert into models (id, name, brand)
values (698, 'Galant', 'Mitsubishi');
insert into models (id, name, brand)
values (699, 'SL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (700, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (701, 'M', 'BMW');
insert into models (id, name, brand)
values (702, 'Exige', 'Lotus');
insert into models (id, name, brand)
values (703, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (704, 'Ciera', 'Oldsmobile');
insert into models (id, name, brand)
values (705, 'Eclipse', 'Mitsubishi');
insert into models (id, name, brand)
values (706, 'Rodeo', 'Isuzu');
insert into models (id, name, brand)
values (707, 'GTI', 'Volkswagen');
insert into models (id, name, brand)
values (708, 'Esprit Turbo', 'Lotus');
insert into models (id, name, brand)
values (709, 'XJ Series', 'Jaguar');
insert into models (id, name, brand)
values (710, 'Bonneville', 'Pontiac');
insert into models (id, name, brand)
values (711, 'Sunfire', 'Pontiac');
insert into models (id, name, brand)
values (712, 'Camry', 'Toyota');
insert into models (id, name, brand)
values (713, 'Stealth', 'Dodge');
insert into models (id, name, brand)
values (714, 'Venture', 'Chevrolet');
insert into models (id, name, brand)
values (715, 'Camry Solara', 'Toyota');
insert into models (id, name, brand)
values (716, 'Chariot', 'Mitsubishi');
insert into models (id, name, brand)
values (717, 'Silverado 2500', 'Chevrolet');
insert into models (id, name, brand)
values (718, '911', 'Porsche');
insert into models (id, name, brand)
values (719, 'Regency', 'Oldsmobile');
insert into models (id, name, brand)
values (720, '90', 'Audi');
insert into models (id, name, brand)
values (721, 'Jetta', 'Volkswagen');
insert into models (id, name, brand)
values (722, 'Ciera', 'Oldsmobile');
insert into models (id, name, brand)
values (723, '2500', 'GMC');
insert into models (id, name, brand)
values (724, 'Camry', 'Toyota');
insert into models (id, name, brand)
values (725, 'Highlander Hybrid', 'Toyota');
insert into models (id, name, brand)
values (726, 'Eldorado', 'Cadillac');
insert into models (id, name, brand)
values (727, 'Beretta', 'Chevrolet');
insert into models (id, name, brand)
values (728, 'Mariner', 'Mercury');
insert into models (id, name, brand)
values (729, 'Escape', 'Ford');
insert into models (id, name, brand)
values (730, 'Talon', 'Eagle');
insert into models (id, name, brand)
values (731, '3 Series', 'BMW');
insert into models (id, name, brand)
values (732, 'ES', 'Lexus');
insert into models (id, name, brand)
values (733, 'SRX', 'Cadillac');
insert into models (id, name, brand)
values (734, 'Cherokee', 'Jeep');
insert into models (id, name, brand)
values (735, 'ES', 'Lexus');
insert into models (id, name, brand)
values (736, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (737, '430 Scuderia', 'Ferrari');
insert into models (id, name, brand)
values (738, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (739, 'Dakota', 'Dodge');
insert into models (id, name, brand)
values (740, 'Firebird', 'Pontiac');
insert into models (id, name, brand)
values (741, 'Avalon', 'Toyota');
insert into models (id, name, brand)
values (742, 'XC70', 'Volvo');
insert into models (id, name, brand)
values (743, 'E350', 'Ford');
insert into models (id, name, brand)
values (744, '944', 'Porsche');
insert into models (id, name, brand)
values (745, 'Esprit', 'Lotus');
insert into models (id, name, brand)
values (746, 'Parisienne', 'Pontiac');
insert into models (id, name, brand)
values (747, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (748, 'Sephia', 'Kia');
insert into models (id, name, brand)
values (749, 'GT', 'Ford');
insert into models (id, name, brand)
values (750, 'Camry Hybrid', 'Toyota');
insert into models (id, name, brand)
values (751, 'GX', 'Lexus');
insert into models (id, name, brand)
values (752, 'Brat', 'Subaru');
insert into models (id, name, brand)
values (753, 'Rabbit', 'Volkswagen');
insert into models (id, name, brand)
values (754, 'Yukon', 'GMC');
insert into models (id, name, brand)
values (755, 'LX', 'Lexus');
insert into models (id, name, brand)
values (756, 'MPV', 'Mazda');
insert into models (id, name, brand)
values (757, 'DB9 Volante', 'Aston Martin');
insert into models (id, name, brand)
values (758, 'J', 'Infiniti');
insert into models (id, name, brand)
values (759, 'Cutlass', 'Oldsmobile');
insert into models (id, name, brand)
values (760, 'A8', 'Audi');
insert into models (id, name, brand)
values (761, 'tC', 'Scion');
insert into models (id, name, brand)
values (762, 'Citation', 'Chevrolet');
insert into models (id, name, brand)
values (763, 'Classic', 'Chevrolet');
insert into models (id, name, brand)
values (764, '525', 'BMW');
insert into models (id, name, brand)
values (765, 'Passport', 'Honda');
insert into models (id, name, brand)
values (766, 'SL-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (767, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (768, 'PT Cruiser', 'Chrysler');
insert into models (id, name, brand)
values (769, 'X3', 'BMW');
insert into models (id, name, brand)
values (770, 'Bonneville', 'Pontiac');
insert into models (id, name, brand)
values (771, 'Impreza', 'Subaru');
insert into models (id, name, brand)
values (772, 'LHS', 'Chrysler');
insert into models (id, name, brand)
values (773, 'Pajero', 'Mitsubishi');
insert into models (id, name, brand)
values (774, 'Diamante', 'Mitsubishi');
insert into models (id, name, brand)
values (775, 'Suburban 1500', 'Chevrolet');
insert into models (id, name, brand)
values (776, 'H3T', 'Hummer');
insert into models (id, name, brand)
values (777, 'Allante', 'Cadillac');
insert into models (id, name, brand)
values (778, 'Sierra 1500', 'GMC');
insert into models (id, name, brand)
values (779, '900', 'Saab');
insert into models (id, name, brand)
values (780, 'RX-8', 'Mazda');
insert into models (id, name, brand)
values (781, 'Prelude', 'Honda');
insert into models (id, name, brand)
values (782, 'Mulsanne', 'Bentley');
insert into models (id, name, brand)
values (783, 'Edge', 'Ford');
insert into models (id, name, brand)
values (784, 'Zephyr', 'Lincoln');
insert into models (id, name, brand)
values (785, 'STS', 'Cadillac');
insert into models (id, name, brand)
values (786, 'Skylark', 'Buick');
insert into models (id, name, brand)
values (787, 'Protege', 'Mazda');
insert into models (id, name, brand)
values (788, 'Taurus', 'Ford');
insert into models (id, name, brand)
values (789, 'Blazer', 'Chevrolet');
insert into models (id, name, brand)
values (790, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (791, 'Focus', 'Ford');
insert into models (id, name, brand)
values (792, 'Accord', 'Honda');
insert into models (id, name, brand)
values (793, 'Esprit', 'Lotus');
insert into models (id, name, brand)
values (794, 'CTS-V', 'Cadillac');
insert into models (id, name, brand)
values (795, 'Mountaineer', 'Mercury');
insert into models (id, name, brand)
values (796, 'GTI', 'Volkswagen');
insert into models (id, name, brand)
values (797, 'Transit Connect', 'Ford');
insert into models (id, name, brand)
values (798, 'Fleetwood', 'Cadillac');
insert into models (id, name, brand)
values (799, 'Durango', 'Dodge');
insert into models (id, name, brand)
values (800, 'Supra', 'Toyota');
insert into models (id, name, brand)
values (801, 'Eclipse', 'Mitsubishi');
insert into models (id, name, brand)
values (802, 'SX4', 'Suzuki');
insert into models (id, name, brand)
values (803, 'CTS', 'Cadillac');
insert into models (id, name, brand)
values (804, 'CR-X', 'Honda');
insert into models (id, name, brand)
values (805, 'Aerostar', 'Ford');
insert into models (id, name, brand)
values (806, 'Avalanche', 'Chevrolet');
insert into models (id, name, brand)
values (807, 'Sedona', 'Kia');
insert into models (id, name, brand)
values (808, 'Escort', 'Ford');
insert into models (id, name, brand)
values (809, 'Skylark', 'Buick');
insert into models (id, name, brand)
values (810, 'TT', 'Audi');
insert into models (id, name, brand)
values (811, 'SRX', 'Cadillac');
insert into models (id, name, brand)
values (812, 'Regal', 'Buick');
insert into models (id, name, brand)
values (813, 'Ram 2500 Club', 'Dodge');
insert into models (id, name, brand)
values (814, 'Routan', 'Volkswagen');
insert into models (id, name, brand)
values (815, 'F-Series Super Duty', 'Ford');
insert into models (id, name, brand)
values (816, 'Colt Vista', 'Plymouth');
insert into models (id, name, brand)
values (817, 'Eclipse', 'Mitsubishi');
insert into models (id, name, brand)
values (818, '9-3', 'Saab');
insert into models (id, name, brand)
values (819, 'Escort', 'Ford');
insert into models (id, name, brand)
values (820, 'Regal', 'Buick');
insert into models (id, name, brand)
values (821, 'Sky', 'Saturn');
insert into models (id, name, brand)
values (822, 'Passat', 'Volkswagen');
insert into models (id, name, brand)
values (823, 'A8', 'Audi');
insert into models (id, name, brand)
values (824, 'A3', 'Audi');
insert into models (id, name, brand)
values (825, 'Riviera', 'Buick');
insert into models (id, name, brand)
values (826, 'Impreza', 'Subaru');
insert into models (id, name, brand)
values (827, 'Galaxie', 'Ford');
insert into models (id, name, brand)
values (828, '62', 'Maybach');
insert into models (id, name, brand)
values (829, 'Contour', 'Ford');
insert into models (id, name, brand)
values (830, 'Swift', 'Suzuki');
insert into models (id, name, brand)
values (831, 'Lancer', 'Mitsubishi');
insert into models (id, name, brand)
values (832, 'MX-5', 'Mazda');
insert into models (id, name, brand)
values (833, 'Yukon XL 1500', 'GMC');
insert into models (id, name, brand)
values (834, 'Silverado 2500', 'Chevrolet');
insert into models (id, name, brand)
values (835, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (836, 'Continental Flying Spur', 'Bentley');
insert into models (id, name, brand)
values (837, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (838, 'Fiero', 'Pontiac');
insert into models (id, name, brand)
values (839, 'MKX', 'Lincoln');
insert into models (id, name, brand)
values (840, 'LeSabre', 'Buick');
insert into models (id, name, brand)
values (841, 'Canyon', 'GMC');
insert into models (id, name, brand)
values (842, 'IS-F', 'Lexus');
insert into models (id, name, brand)
values (843, 'Electra', 'Buick');
insert into models (id, name, brand)
values (844, 'Mustang', 'Ford');
insert into models (id, name, brand)
values (845, 'Sierra 1500', 'GMC');
insert into models (id, name, brand)
values (846, 'Tracker', 'Geo');
insert into models (id, name, brand)
values (847, 'Starion', 'Mitsubishi');
insert into models (id, name, brand)
values (848, 'Trooper', 'Isuzu');
insert into models (id, name, brand)
values (849, 'M5', 'BMW');
insert into models (id, name, brand)
values (850, 'Chariot', 'Mitsubishi');
insert into models (id, name, brand)
values (851, 'Sunbird', 'Pontiac');
insert into models (id, name, brand)
values (852, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (853, 'Maxima', 'Nissan');
insert into models (id, name, brand)
values (854, '300M', 'Chrysler');
insert into models (id, name, brand)
values (855, 'Z3', 'BMW');
insert into models (id, name, brand)
values (856, 'Stratus', 'Dodge');
insert into models (id, name, brand)
values (857, 'Edge', 'Ford');
insert into models (id, name, brand)
values (858, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (859, 'ES', 'Lexus');
insert into models (id, name, brand)
values (860, '2500 Club Coupe', 'GMC');
insert into models (id, name, brand)
values (861, 'SX4', 'Suzuki');
insert into models (id, name, brand)
values (862, 'Envoy', 'GMC');
insert into models (id, name, brand)
values (863, 'Camry', 'Toyota');
insert into models (id, name, brand)
values (864, 'Econoline E350', 'Ford');
insert into models (id, name, brand)
values (865, 'Grand Marquis', 'Mercury');
insert into models (id, name, brand)
values (866, '7 Series', 'BMW');
insert into models (id, name, brand)
values (867, '1500', 'Chevrolet');
insert into models (id, name, brand)
values (868, 'Bravada', 'Oldsmobile');
insert into models (id, name, brand)
values (869, 'ES', 'Lexus');
insert into models (id, name, brand)
values (870, 'Safari', 'GMC');
insert into models (id, name, brand)
values (871, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (872, '7 Series', 'BMW');
insert into models (id, name, brand)
values (873, 'Bronco', 'Ford');
insert into models (id, name, brand)
values (874, 'Range Rover Sport', 'Land Rover');
insert into models (id, name, brand)
values (875, 'Grand Prix', 'Pontiac');
insert into models (id, name, brand)
values (876, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (877, 'G', 'Infiniti');
insert into models (id, name, brand)
values (878, 'Envoy', 'GMC');
insert into models (id, name, brand)
values (879, 'Impreza', 'Subaru');
insert into models (id, name, brand)
values (880, 'A4', 'Audi');
insert into models (id, name, brand)
values (881, 'MX-6', 'Mazda');
insert into models (id, name, brand)
values (882, 'Jetta', 'Volkswagen');
insert into models (id, name, brand)
values (883, 'Explorer', 'Ford');
insert into models (id, name, brand)
values (884, 'F450', 'Ford');
insert into models (id, name, brand)
values (885, 'Forenza', 'Suzuki');
insert into models (id, name, brand)
values (886, 'W201', 'Mercedes-Benz');
insert into models (id, name, brand)
values (887, 'Cressida', 'Toyota');
insert into models (id, name, brand)
values (888, 'E-Series', 'Ford');
insert into models (id, name, brand)
values (889, 'Range Rover', 'Land Rover');
insert into models (id, name, brand)
values (890, 'CLK-Class', 'Mercedes-Benz');
insert into models (id, name, brand)
values (891, 'Savana 3500', 'GMC');
insert into models (id, name, brand)
values (892, 'X3', 'BMW');
insert into models (id, name, brand)
values (893, 'Insight', 'Honda');
insert into models (id, name, brand)
values (894, 'Ciera', 'Oldsmobile');
insert into models (id, name, brand)
values (895, 'Camaro', 'Chevrolet');
insert into models (id, name, brand)
values (896, 'Neon', 'Dodge');
insert into models (id, name, brand)
values (897, 'Avalanche 2500', 'Chevrolet');
insert into models (id, name, brand)
values (898, 'F350', 'Ford');
insert into models (id, name, brand)
values (899, '2500', 'Chevrolet');
insert into models (id, name, brand)
values (900, 'Sable', 'Mercury');
insert into models (id, name, brand)
values (901, 'Taurus', 'Ford');
insert into models (id, name, brand)
values (902, 'DTS', 'Cadillac');
insert into models (id, name, brand)
values (903, 'S6', 'Audi');
insert into models (id, name, brand)
values (904, 'Q', 'Infiniti');
insert into models (id, name, brand)
values (905, 'E-350 Super Duty Van', 'Ford');
insert into models (id, name, brand)
values (906, 'Passat', 'Volkswagen');
insert into models (id, name, brand)
values (907, 'Fit', 'Honda');
insert into models (id, name, brand)
values (908, 'Capri', 'Mercury');
insert into models (id, name, brand)
values (909, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (910, 'ES', 'Lexus');
insert into models (id, name, brand)
values (911, 'Boxster', 'Porsche');
insert into models (id, name, brand)
values (912, '626', 'Mazda');
insert into models (id, name, brand)
values (913, 'Tahoe', 'Chevrolet');
insert into models (id, name, brand)
values (914, 'SC', 'Lexus');
insert into models (id, name, brand)
values (915, '530', 'BMW');
insert into models (id, name, brand)
values (916, 'Savana 2500', 'GMC');
insert into models (id, name, brand)
values (917, 'Tracker', 'Geo');
insert into models (id, name, brand)
values (918, 'S10', 'Chevrolet');
insert into models (id, name, brand)
values (919, 'Ram 3500 Club', 'Dodge');
insert into models (id, name, brand)
values (920, 'GS', 'Lexus');
insert into models (id, name, brand)
values (921, '98', 'Oldsmobile');
insert into models (id, name, brand)
values (922, 'G-Series G10', 'Chevrolet');
insert into models (id, name, brand)
values (923, 'Savana 1500', 'GMC');
insert into models (id, name, brand)
values (924, 'E-Series', 'Ford');
insert into models (id, name, brand)
values (925, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (926, '3500 Club Coupe', 'GMC');
insert into models (id, name, brand)
values (927, '9-5', 'Saab');
insert into models (id, name, brand)
values (928, 'RL', 'Acura');
insert into models (id, name, brand)
values (929, 'IS', 'Lexus');
insert into models (id, name, brand)
values (930, 'Express 2500', 'Chevrolet');
insert into models (id, name, brand)
values (931, 'Ram Van 3500', 'Dodge');
insert into models (id, name, brand)
values (932, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (933, 'Ranger', 'Ford');
insert into models (id, name, brand)
values (934, 'X3', 'BMW');
insert into models (id, name, brand)
values (935, 'Mirage', 'Mitsubishi');
insert into models (id, name, brand)
values (936, 'E-Series', 'Ford');
insert into models (id, name, brand)
values (937, 'Silverado', 'Chevrolet');
insert into models (id, name, brand)
values (938, 'Eldorado', 'Cadillac');
insert into models (id, name, brand)
values (939, 'Silverado', 'Chevrolet');
insert into models (id, name, brand)
values (940, 'Expedition EL', 'Ford');
insert into models (id, name, brand)
values (941, 'STS', 'Cadillac');
insert into models (id, name, brand)
values (942, 'Tundra', 'Toyota');
insert into models (id, name, brand)
values (943, 'Corvair 500', 'Chevrolet');
insert into models (id, name, brand)
values (944, 'Esteem', 'Suzuki');
insert into models (id, name, brand)
values (945, 'Daewoo Lacetti', 'Suzuki');
insert into models (id, name, brand)
values (946, 'CTS', 'Cadillac');
insert into models (id, name, brand)
values (947, 'Sentra', 'Nissan');
insert into models (id, name, brand)
values (948, '57', 'Maybach');
insert into models (id, name, brand)
values (949, 'Ghost', 'Rolls-Royce');
insert into models (id, name, brand)
values (950, 'Focus', 'Ford');
insert into models (id, name, brand)
values (951, 'X5', 'BMW');
insert into models (id, name, brand)
values (952, 'Fillmore', 'Fillmore');
insert into models (id, name, brand)
values (953, 'Stratus', 'Dodge');
insert into models (id, name, brand)
values (954, 'Skyhawk', 'Buick');
insert into models (id, name, brand)
values (955, 'LeSabre', 'Buick');
insert into models (id, name, brand)
values (956, 'CTS', 'Cadillac');
insert into models (id, name, brand)
values (957, 'Topaz', 'Mercury');
insert into models (id, name, brand)
values (958, 'Ram 2500', 'Dodge');
insert into models (id, name, brand)
values (959, 'Elantra', 'Hyundai');
insert into models (id, name, brand)
values (960, 'Storm', 'Geo');
insert into models (id, name, brand)
values (961, 'C70', 'Volvo');
insert into models (id, name, brand)
values (962, 'Fusion', 'Ford');
insert into models (id, name, brand)
values (963, 'Ram Wagon B150', 'Dodge');
insert into models (id, name, brand)
values (964, 'XC90', 'Volvo');
insert into models (id, name, brand)
values (965, 'Allante', 'Cadillac');
insert into models (id, name, brand)
values (966, 'Range Rover Evoque', 'Land Rover');
insert into models (id, name, brand)
values (967, 'Discovery Series II', 'Land Rover');
insert into models (id, name, brand)
values (968, 'DBS', 'Aston Martin');
insert into models (id, name, brand)
values (969, 'Armada', 'Nissan');
insert into models (id, name, brand)
values (970, 'GTO', 'Pontiac');
insert into models (id, name, brand)
values (971, 'Escalade ESV', 'Cadillac');
insert into models (id, name, brand)
values (972, '911', 'Porsche');
insert into models (id, name, brand)
values (973, '900', 'Saab');
insert into models (id, name, brand)
values (974, '5000S', 'Audi');
insert into models (id, name, brand)
values (975, 'Grand Caravan', 'Dodge');
insert into models (id, name, brand)
values (976, 'Regal', 'Buick');
insert into models (id, name, brand)
values (977, 'New Yorker', 'Chrysler');
insert into models (id, name, brand)
values (978, 'XF', 'Jaguar');
insert into models (id, name, brand)
values (979, 'Type 2', 'Volkswagen');
insert into models (id, name, brand)
values (980, 'GT500', 'Ford');
insert into models (id, name, brand)
values (981, 'Cobalt', 'Chevrolet');
insert into models (id, name, brand)
values (982, 'Navigator', 'Lincoln');
insert into models (id, name, brand)
values (983, 'TL', 'Acura');
insert into models (id, name, brand)
values (984, 'Cooper Clubman', 'MINI');
insert into models (id, name, brand)
values (985, '2500', 'GMC');
insert into models (id, name, brand)
values (986, 'RVR', 'Mitsubishi');
insert into models (id, name, brand)
values (987, '8 Series', 'BMW');
insert into models (id, name, brand)
values (988, 'Savana 3500', 'GMC');
insert into models (id, name, brand)
values (989, 'Stratus', 'Dodge');
insert into models (id, name, brand)
values (990, 'Sunbird', 'Pontiac');
insert into models (id, name, brand)
values (991, 'V70', 'Volvo');
insert into models (id, name, brand)
values (992, 'X6', 'BMW');
insert into models (id, name, brand)
values (993, 'Civic', 'Honda');
insert into models (id, name, brand)
values (994, 'Lucerne', 'Buick');
insert into models (id, name, brand)
values (995, 'Thunderbird', 'Ford');
insert into models (id, name, brand)
values (996, '200', 'Audi');
insert into models (id, name, brand)
values (997, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (998, 'Rodeo', 'Isuzu');
insert into models (id, name, brand)
values (999, 'Wrangler', 'Jeep');
insert into models (id, name, brand)
values (1000, 'DeVille', 'Cadillac');
