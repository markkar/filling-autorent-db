insert into pickup_places (id, country, city, street, number)
values (1, 'United States', 'Clearwater', 'Park Meadow', '960');
insert into pickup_places (id, country, city, street, number)
values (2, 'China', 'Shuangxi', 'Bashford', '729');
insert into pickup_places (id, country, city, street, number)
values (3, 'United States', 'San Diego', 'Miller', '67849');
insert into pickup_places (id, country, city, street, number)
values (4, 'Brazil', 'Nanuque', 'Nevada', '24211');
insert into pickup_places (id, country, city, street, number)
values (5, 'Panama', 'Jaqué', 'Spaight', '3893');
insert into pickup_places (id, country, city, street, number)
values (6, 'Croatia', 'Pridraga', 'Old Gate', '6');
insert into pickup_places (id, country, city, street, number)
values (7, 'Poland', 'Zbąszyń', 'Sommers', '82269');
insert into pickup_places (id, country, city, street, number)
values (8, 'China', 'Xiliguantun', 'Debra', '9');
insert into pickup_places (id, country, city, street, number)
values (9, 'Colombia', 'Líbano', 'Anhalt', '130');
insert into pickup_places (id, country, city, street, number)
values (10, 'Portugal', 'Vila Alva', 'Chinook', '4976');
insert into pickup_places (id, country, city, street, number)
values (11, 'Russia', 'Partizansk', 'Maple Wood', '31');
insert into pickup_places (id, country, city, street, number)
values (12, 'United States', 'Saint Paul', 'Barby', '5');
insert into pickup_places (id, country, city, street, number)
values (13, 'Poland', 'Łaszczów', 'Dovetail', '33');
insert into pickup_places (id, country, city, street, number)
values (14, 'Russia', 'Teberda', 'Bay', '97151');
insert into pickup_places (id, country, city, street, number)
values (15, 'Russia', 'Psebay', 'Manitowish', '3165');
insert into pickup_places (id, country, city, street, number)
values (16, 'United States', 'Melbourne', 'Paget', '6935');
insert into pickup_places (id, country, city, street, number)
values (17, 'Japan', 'Tomakomai', 'Hauk', '6429');
insert into pickup_places (id, country, city, street, number)
values (18, 'Portugal', 'Martingança-Gare', 'Corben', '4746');
insert into pickup_places (id, country, city, street, number)
values (19, 'Poland', 'Broniszewice', 'Westridge', '2');
insert into pickup_places (id, country, city, street, number)
values (20, 'Denmark', 'København', 'Pierstorff', '06159');
insert into pickup_places (id, country, city, street, number)
values (21, 'Indonesia', 'Sumberbatas', 'Luster', '68229');
insert into pickup_places (id, country, city, street, number)
values (22, 'United States', 'Dayton', 'Bartelt', '21');
insert into pickup_places (id, country, city, street, number)
values (23, 'Ghana', 'Winneba', 'Gateway', '4');
insert into pickup_places (id, country, city, street, number)
values (24, 'Saint Vincent and the Grenadines', 'Chateaubelair', 'Maywood', '1');
insert into pickup_places (id, country, city, street, number)
values (25, 'Brazil', 'Carandaí', 'Sachtjen', '24');
insert into pickup_places (id, country, city, street, number)
values (26, 'Russia', 'Novaya Mayna', 'Ohio', '8671');
insert into pickup_places (id, country, city, street, number)
values (27, 'Indonesia', 'Oemofa', 'Debra', '5626');
insert into pickup_places (id, country, city, street, number)
values (28, 'Malaysia', 'Kota Bharu', 'Namekagon', '478');
insert into pickup_places (id, country, city, street, number)
values (29, 'Argentina', 'Gobernador Ingeniero Valentín Virasoro', 'Badeau', '36');
insert into pickup_places (id, country, city, street, number)
values (30, 'Indonesia', 'Pulosari', 'Dryden', '66245');
insert into pickup_places (id, country, city, street, number)
values (31, 'Brazil', 'Coromandel', 'Rutledge', '773');
insert into pickup_places (id, country, city, street, number)
values (32, 'Ghana', 'Mamponteng', 'Del Sol', '057');
insert into pickup_places (id, country, city, street, number)
values (33, 'Mongolia', 'Bayshint', 'Stang', '35');
insert into pickup_places (id, country, city, street, number)
values (34, 'United States', 'Ogden', 'Monica', '00');
insert into pickup_places (id, country, city, street, number)
values (35, 'Poland', 'Dębów', 'Lerdahl', '84368');
insert into pickup_places (id, country, city, street, number)
values (36, 'Libya', 'Tagiura', 'Sauthoff', '12388');
insert into pickup_places (id, country, city, street, number)
values (37, 'El Salvador', 'Santa Ana', 'Havey', '10116');
insert into pickup_places (id, country, city, street, number)
values (38, 'China', 'Gaoxiang', 'Northwestern', '3035');
insert into pickup_places (id, country, city, street, number)
values (39, 'China', 'Xifangcheng', 'Clyde Gallagher', '3296');
insert into pickup_places (id, country, city, street, number)
values (40, 'Brazil', 'Mandaguari', 'Anthes', '92');
insert into pickup_places (id, country, city, street, number)
values (41, 'France', 'Troyes', 'Columbus', '99');
insert into pickup_places (id, country, city, street, number)
values (42, 'Brazil', 'Butiá', 'Pleasure', '64778');
insert into pickup_places (id, country, city, street, number)
values (43, 'Greece', 'Kyparissía', 'Haas', '280');
insert into pickup_places (id, country, city, street, number)
values (44, 'Philippines', 'Salvacion', 'Huxley', '21');
insert into pickup_places (id, country, city, street, number)
values (45, 'Russia', 'Nizhnepavlovka', 'Loftsgordon', '5177');
insert into pickup_places (id, country, city, street, number)
values (46, 'Philippines', 'Loay', 'Twin Pines', '608');
insert into pickup_places (id, country, city, street, number)
values (47, 'France', 'Tournan-en-Brie', 'Pennsylvania', '7163');
insert into pickup_places (id, country, city, street, number)
values (48, 'South Africa', 'Matatiele', 'Grasskamp', '18');
insert into pickup_places (id, country, city, street, number)
values (49, 'Thailand', 'Nakhon Chai Si', 'Springs', '6');
insert into pickup_places (id, country, city, street, number)
values (50, 'Norway', 'Skien', 'Cardinal', '91528');
insert into pickup_places (id, country, city, street, number)
values (51, 'China', 'Hongling', 'Lyons', '96');
insert into pickup_places (id, country, city, street, number)
values (52, 'Greece', 'Vári', 'Ohio', '0307');
insert into pickup_places (id, country, city, street, number)
values (53, 'China', 'Yanhong', 'Sundown', '6');
insert into pickup_places (id, country, city, street, number)
values (54, 'Indonesia', 'Sleman', 'Ohio', '998');
insert into pickup_places (id, country, city, street, number)
values (55, 'Vietnam', 'Thị Trấn Phú Mỹ', 'Cambridge', '19');
insert into pickup_places (id, country, city, street, number)
values (56, 'Poland', 'Ursus', 'Fair Oaks', '78');
insert into pickup_places (id, country, city, street, number)
values (57, 'Botswana', 'Gaborone', '7th', '35896');
insert into pickup_places (id, country, city, street, number)
values (58, 'Philippines', 'Lagawe', 'Towne', '60');
insert into pickup_places (id, country, city, street, number)
values (59, 'Indonesia', 'Windusakti Hilir', 'Sherman', '63');
insert into pickup_places (id, country, city, street, number)
values (60, 'China', 'Dadong', 'Rusk', '5');
insert into pickup_places (id, country, city, street, number)
values (61, 'China', 'Bayan Hot', 'Bartillon', '367');
insert into pickup_places (id, country, city, street, number)
values (62, 'Malaysia', 'Kuala Lumpur', 'Logan', '96277');
insert into pickup_places (id, country, city, street, number)
values (63, 'Indonesia', 'Angan', 'Colorado', '2144');
insert into pickup_places (id, country, city, street, number)
values (64, 'United States', 'Greensboro', 'Service', '77');
insert into pickup_places (id, country, city, street, number)
values (65, 'Norway', 'Oslo', 'Grayhawk', '84');
insert into pickup_places (id, country, city, street, number)
values (66, 'United States', 'Delray Beach', 'Bartillon', '5029');
insert into pickup_places (id, country, city, street, number)
values (67, 'Poland', 'Janowice', 'Pine View', '90247');
insert into pickup_places (id, country, city, street, number)
values (68, 'China', 'Sanchahe', 'Ruskin', '90569');
insert into pickup_places (id, country, city, street, number)
values (69, 'Indonesia', 'Kalapagada', 'Summer Ridge', '32');
insert into pickup_places (id, country, city, street, number)
values (70, 'Portugal', 'Moses', 'Riverside', '3');
insert into pickup_places (id, country, city, street, number)
values (71, 'Kyrgyzstan', 'Iradan', 'Northland', '09');
insert into pickup_places (id, country, city, street, number)
values (72, 'Poland', 'Wojnicz', 'Little Fleur', '461');
insert into pickup_places (id, country, city, street, number)
values (73, 'Portugal', 'Quintães', 'Forest Run', '1564');
insert into pickup_places (id, country, city, street, number)
values (74, 'Pakistan', 'Basīrpur', 'Algoma', '9');
insert into pickup_places (id, country, city, street, number)
values (75, 'Indonesia', 'Cikiray', 'Oneill', '02896');
insert into pickup_places (id, country, city, street, number)
values (76, 'Slovenia', 'Leskovec pri Krškem', 'Portage', '41');
insert into pickup_places (id, country, city, street, number)
values (77, 'Macedonia', 'Ilinden', 'Center', '8');
insert into pickup_places (id, country, city, street, number)
values (78, 'Indonesia', 'Curahkalak Tengah', 'Cordelia', '52228');
insert into pickup_places (id, country, city, street, number)
values (79, 'China', 'Xizhong', 'Alpine', '407');
insert into pickup_places (id, country, city, street, number)
values (80, 'China', 'Heichengzi', 'Jana', '58068');
insert into pickup_places (id, country, city, street, number)
values (81, 'China', 'Shuntian', 'Leroy', '012');
insert into pickup_places (id, country, city, street, number)
values (82, 'China', 'Wangji', 'Graedel', '77');
insert into pickup_places (id, country, city, street, number)
values (83, 'Kenya', 'Malikisi', 'Cardinal', '83');
insert into pickup_places (id, country, city, street, number)
values (84, 'Russia', 'Kazinka', 'Northport', '5');
insert into pickup_places (id, country, city, street, number)
values (85, 'China', 'Gaoping', 'Dakota', '2718');
insert into pickup_places (id, country, city, street, number)
values (86, 'Portugal', 'Pontinha', 'Sloan', '0');
insert into pickup_places (id, country, city, street, number)
values (87, 'Iran', 'Jīroft', 'Dayton', '0101');
insert into pickup_places (id, country, city, street, number)
values (88, 'China', 'Baitu', 'Kenwood', '113');
insert into pickup_places (id, country, city, street, number)
values (89, 'Russia', 'Amga', 'Luster', '61176');
insert into pickup_places (id, country, city, street, number)
values (90, 'China', 'Shetangpo', 'Gina', '22');
insert into pickup_places (id, country, city, street, number)
values (91, 'China', 'Altunemil', 'Shasta', '10');
insert into pickup_places (id, country, city, street, number)
values (92, 'France', 'Nantes', 'Delaware', '44');
insert into pickup_places (id, country, city, street, number)
values (93, 'Japan', 'Nagaoka', 'Jenna', '4611');
insert into pickup_places (id, country, city, street, number)
values (94, 'Russia', 'Shestakovo', 'Mcguire', '2886');
insert into pickup_places (id, country, city, street, number)
values (95, 'China', 'Zhuhu', 'Gerald', '3641');
insert into pickup_places (id, country, city, street, number)
values (96, 'Poland', 'Stryszawa', 'Farmco', '81');
insert into pickup_places (id, country, city, street, number)
values (97, 'China', 'Baiyanghe', 'Meadow Valley', '98');
insert into pickup_places (id, country, city, street, number)
values (98, 'China', 'Yucheng', 'Village Green', '34502');
insert into pickup_places (id, country, city, street, number)
values (99, 'Poland', 'Polanica-Zdrój', 'Westport', '9530');
insert into pickup_places (id, country, city, street, number)
values (100, 'Czech Republic', 'Brodek u Přerova', 'Prairie Rose', '42');
insert into pickup_places (id, country, city, street, number)
values (101, 'Indonesia', 'Batang', 'Dahle', '98276');
insert into pickup_places (id, country, city, street, number)
values (102, 'Greece', 'Koufália', 'Lillian', '40883');
insert into pickup_places (id, country, city, street, number)
values (103, 'Indonesia', 'Sungaikakap', 'Bunting', '92010');
insert into pickup_places (id, country, city, street, number)
values (104, 'Indonesia', 'Jembayan Hitam', 'Transport', '17');
insert into pickup_places (id, country, city, street, number)
values (105, 'Portugal', 'Abade de Neiva', 'Chinook', '17837');
insert into pickup_places (id, country, city, street, number)
values (106, 'Portugal', 'Aviúges', 'Summit', '60940');
insert into pickup_places (id, country, city, street, number)
values (107, 'China', 'Gexi', 'Derek', '52');
insert into pickup_places (id, country, city, street, number)
values (108, 'China', 'Huanggangshan', 'Del Sol', '00444');
insert into pickup_places (id, country, city, street, number)
values (109, 'Poland', 'Pruszcz', 'Cambridge', '130');
insert into pickup_places (id, country, city, street, number)
values (110, 'Portugal', 'Santa Bárbara de Padrões', 'Saint Paul', '888');
insert into pickup_places (id, country, city, street, number)
values (111, 'China', 'Liuzhou', 'Doe Crossing', '64');
insert into pickup_places (id, country, city, street, number)
values (112, 'Brazil', 'Viçosa', 'Milwaukee', '435');
insert into pickup_places (id, country, city, street, number)
values (113, 'New Zealand', 'Tairua', 'Commercial', '290');
insert into pickup_places (id, country, city, street, number)
values (114, 'Philippines', 'Odiong', 'Elgar', '87176');
insert into pickup_places (id, country, city, street, number)
values (115, 'Costa Rica', 'Matina', 'Goodland', '97279');
insert into pickup_places (id, country, city, street, number)
values (116, 'Malaysia', 'Pulau Pinang', 'Dennis', '02');
insert into pickup_places (id, country, city, street, number)
values (117, 'Russia', 'Ozërsk', 'Kings', '9');
insert into pickup_places (id, country, city, street, number)
values (118, 'Portugal', 'Cachadinha', 'Pawling', '38294');
insert into pickup_places (id, country, city, street, number)
values (119, 'Indonesia', 'Lajaluhur', 'Sachtjen', '659');
insert into pickup_places (id, country, city, street, number)
values (120, 'Brazil', 'Araçoiaba da Serra', 'Evergreen', '2');
insert into pickup_places (id, country, city, street, number)
values (121, 'Philippines', 'Madrid', 'Arkansas', '86116');
insert into pickup_places (id, country, city, street, number)
values (122, 'Greece', 'Sérifos', 'Shoshone', '01768');
insert into pickup_places (id, country, city, street, number)
values (123, 'Honduras', 'El Lolo', 'Dovetail', '956');
insert into pickup_places (id, country, city, street, number)
values (124, 'Indonesia', 'Muting', 'Heath', '337');
insert into pickup_places (id, country, city, street, number)
values (125, 'Vietnam', 'Chợ Chu', '3rd', '16');
insert into pickup_places (id, country, city, street, number)
values (126, 'Peru', 'Capacmarca', 'Debs', '4091');
insert into pickup_places (id, country, city, street, number)
values (127, 'Botswana', 'Makoba', 'Birchwood', '645');
insert into pickup_places (id, country, city, street, number)
values (128, 'Indonesia', 'Palla', 'Gina', '98115');
insert into pickup_places (id, country, city, street, number)
values (129, 'Philippines', 'San Juan', 'Pepper Wood', '0');
insert into pickup_places (id, country, city, street, number)
values (130, 'China', 'Shijiazhai', 'Jay', '5');
insert into pickup_places (id, country, city, street, number)
values (131, 'Sweden', 'Enköping', 'Eastwood', '3167');
insert into pickup_places (id, country, city, street, number)
values (132, 'Russia', 'Atlasovo', 'Pawling', '50975');
insert into pickup_places (id, country, city, street, number)
values (133, 'Philippines', 'Bobonan', 'Bunker Hill', '3');
insert into pickup_places (id, country, city, street, number)
values (134, 'Uganda', 'Kitamilo', 'Hayes', '62');
insert into pickup_places (id, country, city, street, number)
values (135, 'Sweden', 'Göteborg', 'Gerald', '59');
insert into pickup_places (id, country, city, street, number)
values (136, 'South Africa', 'Tembisa', 'Arkansas', '937');
insert into pickup_places (id, country, city, street, number)
values (137, 'Philippines', 'Gabi', 'Merry', '3752');
insert into pickup_places (id, country, city, street, number)
values (138, 'United States', 'Nashville', 'Cherokee', '0');
insert into pickup_places (id, country, city, street, number)
values (139, 'Philippines', 'Infanta', 'Marquette', '71');
insert into pickup_places (id, country, city, street, number)
values (140, 'Thailand', 'Sathon', 'Transport', '893');
insert into pickup_places (id, country, city, street, number)
values (141, 'Russia', 'Verkhozim', 'Prairie Rose', '459');
insert into pickup_places (id, country, city, street, number)
values (142, 'Ethiopia', 'Dodola', 'Clove', '34');
insert into pickup_places (id, country, city, street, number)
values (143, 'Netherlands', 'Hengelo', 'Golf Course', '819');
insert into pickup_places (id, country, city, street, number)
values (144, 'Canada', 'Smiths Falls', 'Fordem', '68296');
insert into pickup_places (id, country, city, street, number)
values (145, 'China', 'Fenchen', 'Ramsey', '8');
insert into pickup_places (id, country, city, street, number)
values (146, 'Indonesia', 'Bhokadoke', 'Sheridan', '0067');
insert into pickup_places (id, country, city, street, number)
values (147, 'China', 'Hekou', 'Brickson Park', '1');
insert into pickup_places (id, country, city, street, number)
values (148, 'China', 'Rixinhe', 'Anthes', '17');
insert into pickup_places (id, country, city, street, number)
values (149, 'Czech Republic', 'Hrabyně', 'Sherman', '86');
insert into pickup_places (id, country, city, street, number)
values (150, 'Japan', 'Nagai', 'Swallow', '5717');
insert into pickup_places (id, country, city, street, number)
values (151, 'Russia', 'Kuzovatovo', 'Shasta', '79');
insert into pickup_places (id, country, city, street, number)
values (152, 'Iran', 'Shaft', 'Buhler', '3');
insert into pickup_places (id, country, city, street, number)
values (153, 'Egypt', 'Alexandria', 'Wayridge', '3');
insert into pickup_places (id, country, city, street, number)
values (154, 'Portugal', 'Palmeiros', 'Manitowish', '9');
insert into pickup_places (id, country, city, street, number)
values (155, 'Peru', 'El Alto', 'Miller', '060');
insert into pickup_places (id, country, city, street, number)
values (156, 'France', 'Molsheim', 'Mockingbird', '3');
insert into pickup_places (id, country, city, street, number)
values (157, 'Kosovo', 'Zubin Potok', 'Arizona', '5');
insert into pickup_places (id, country, city, street, number)
values (158, 'Pakistan', 'Mailsi', 'Shelley', '036');
insert into pickup_places (id, country, city, street, number)
values (159, 'Brazil', 'Ribeirão Pires', 'Buhler', '84212');
insert into pickup_places (id, country, city, street, number)
values (160, 'Colombia', 'San Gil', 'Amoth', '5');
insert into pickup_places (id, country, city, street, number)
values (161, 'Indonesia', 'Awarawar', 'School', '0');
insert into pickup_places (id, country, city, street, number)
values (162, 'Czech Republic', 'Vidče', 'Caliangt', '402');
insert into pickup_places (id, country, city, street, number)
values (163, 'Venezuela', 'Porlamar', 'Memorial', '12461');
insert into pickup_places (id, country, city, street, number)
values (164, 'Mayotte', 'Ouangani', 'Nancy', '632');
insert into pickup_places (id, country, city, street, number)
values (165, 'China', 'Jiaocun', 'Derek', '91');
insert into pickup_places (id, country, city, street, number)
values (166, 'Canada', 'Cowansville', 'Oakridge', '04');
insert into pickup_places (id, country, city, street, number)
values (167, 'Indonesia', 'Modosinal', 'Holmberg', '9');
insert into pickup_places (id, country, city, street, number)
values (168, 'Thailand', 'Pa Mok', 'Huxley', '7601');
insert into pickup_places (id, country, city, street, number)
values (169, 'Comoros', 'Sima', 'Lindbergh', '54');
insert into pickup_places (id, country, city, street, number)
values (170, 'China', 'Guanshan', 'Forest Run', '19');
insert into pickup_places (id, country, city, street, number)
values (171, 'Czech Republic', 'Kamenický Šenov', 'Independence', '2');
insert into pickup_places (id, country, city, street, number)
values (172, 'Canada', 'Grand Bank', 'West', '1');
insert into pickup_places (id, country, city, street, number)
values (173, 'China', 'Tianzhou', 'Steensland', '397');
insert into pickup_places (id, country, city, street, number)
values (174, 'Belarus', 'Pyetrykaw', 'Sycamore', '305');
insert into pickup_places (id, country, city, street, number)
values (175, 'Croatia', 'Opuzen', 'Spenser', '2');
insert into pickup_places (id, country, city, street, number)
values (176, 'Japan', 'Hatogaya-honchō', 'Bluestem', '56');
insert into pickup_places (id, country, city, street, number)
values (177, 'Poland', 'Niemodlin', 'Nova', '92');
insert into pickup_places (id, country, city, street, number)
values (178, 'Argentina', 'Villa Ojo de Agua', 'Mendota', '2646');
insert into pickup_places (id, country, city, street, number)
values (179, 'Philippines', 'Talospatang', 'Sutherland', '1872');
insert into pickup_places (id, country, city, street, number)
values (180, 'Honduras', 'San Juan de Planes', 'Heffernan', '4');
insert into pickup_places (id, country, city, street, number)
values (181, 'Russia', 'Barnaul', 'Anthes', '8');
insert into pickup_places (id, country, city, street, number)
values (182, 'China', 'Zongzhai', 'Delladonna', '4940');
insert into pickup_places (id, country, city, street, number)
values (183, 'China', 'Chengcun', 'Shopko', '85322');
insert into pickup_places (id, country, city, street, number)
values (184, 'Russia', 'Novopokrovka', 'Village', '1');
insert into pickup_places (id, country, city, street, number)
values (185, 'China', 'Shengli', 'Walton', '1375');
insert into pickup_places (id, country, city, street, number)
values (186, 'Estonia', 'Kuressaare', 'Norway Maple', '2');
insert into pickup_places (id, country, city, street, number)
values (187, 'Indonesia', 'Sidokumpul', 'Dixon', '6397');
insert into pickup_places (id, country, city, street, number)
values (188, 'Indonesia', 'Lela', 'Becker', '9');
insert into pickup_places (id, country, city, street, number)
values (189, 'Thailand', 'Na Tan', 'Del Mar', '63272');
insert into pickup_places (id, country, city, street, number)
values (190, 'Indonesia', 'Naifalo', 'Haas', '37');
insert into pickup_places (id, country, city, street, number)
values (191, 'Indonesia', 'Sumberpitu', 'Lillian', '619');
insert into pickup_places (id, country, city, street, number)
values (192, 'China', 'Lidong', 'Forster', '1294');
insert into pickup_places (id, country, city, street, number)
values (193, 'Niger', 'Gaya', 'Roth', '34509');
insert into pickup_places (id, country, city, street, number)
values (194, 'Portugal', 'Ferreiras', 'Sauthoff', '4022');
insert into pickup_places (id, country, city, street, number)
values (195, 'Russia', 'Strel''na', 'Hagan', '4703');
insert into pickup_places (id, country, city, street, number)
values (196, 'Indonesia', 'Pangalangan', 'Green Ridge', '8');
insert into pickup_places (id, country, city, street, number)
values (197, 'China', 'Kangping', 'Hazelcrest', '768');
insert into pickup_places (id, country, city, street, number)
values (198, 'South Africa', 'Moorreesburg', 'Bobwhite', '52055');
insert into pickup_places (id, country, city, street, number)
values (199, 'China', 'Lingtou', 'Del Mar', '8');
insert into pickup_places (id, country, city, street, number)
values (200, 'Palestinian Territory', 'Al Fandaqūmīyah', 'Parkside', '071');
insert into pickup_places (id, country, city, street, number)
values (201, 'Russia', 'Sofrino', 'Scofield', '7061');
insert into pickup_places (id, country, city, street, number)
values (202, 'Indonesia', 'Gununganyar', 'Darwin', '2');
insert into pickup_places (id, country, city, street, number)
values (203, 'Brazil', 'Rio Bonito', 'Redwing', '21');
insert into pickup_places (id, country, city, street, number)
values (204, 'Venezuela', 'Maroa', 'Canary', '42830');
insert into pickup_places (id, country, city, street, number)
values (205, 'United States', 'Indianapolis', 'Cardinal', '346');
insert into pickup_places (id, country, city, street, number)
values (206, 'China', 'La’ershan', 'Haas', '5');
insert into pickup_places (id, country, city, street, number)
values (207, 'Japan', 'Shisui', 'Oneill', '1814');
insert into pickup_places (id, country, city, street, number)
values (208, 'Brazil', 'Joinville', 'Northwestern', '1103');
insert into pickup_places (id, country, city, street, number)
values (209, 'Thailand', 'Borabue', 'Crest Line', '192');
insert into pickup_places (id, country, city, street, number)
values (210, 'Vietnam', 'Tô Hạp', 'Melvin', '19');
insert into pickup_places (id, country, city, street, number)
values (211, 'Philippines', 'Kotkot', 'Hazelcrest', '440');
insert into pickup_places (id, country, city, street, number)
values (212, 'Russia', 'Sabnova', 'Bowman', '178');
insert into pickup_places (id, country, city, street, number)
values (213, 'China', 'Longqiao', 'Esker', '8');
insert into pickup_places (id, country, city, street, number)
values (214, 'Canada', 'Trail', 'Dahle', '93612');
insert into pickup_places (id, country, city, street, number)
values (215, 'Indonesia', 'Sukasenang', 'Moulton', '4919');
insert into pickup_places (id, country, city, street, number)
values (216, 'Portugal', 'Aroeira', 'Rowland', '0');
insert into pickup_places (id, country, city, street, number)
values (217, 'Chad', 'Bokoro', 'Hallows', '5470');
insert into pickup_places (id, country, city, street, number)
values (218, 'Gabon', 'Port-Gentil', 'Rieder', '487');
insert into pickup_places (id, country, city, street, number)
values (219, 'China', 'Yaogu', 'Melody', '37');
insert into pickup_places (id, country, city, street, number)
values (220, 'Colombia', 'Yopal', 'Rieder', '0263');
insert into pickup_places (id, country, city, street, number)
values (221, 'Afghanistan', '’Unābah', 'Aberg', '5391');
insert into pickup_places (id, country, city, street, number)
values (222, 'Philippines', 'Cabangahan', 'Hollow Ridge', '06510');
insert into pickup_places (id, country, city, street, number)
values (223, 'Russia', 'Strogino', 'Vahlen', '675');
insert into pickup_places (id, country, city, street, number)
values (224, 'Palestinian Territory', 'Yāsūf', 'Cody', '3955');
insert into pickup_places (id, country, city, street, number)
values (225, 'Peru', 'Acolla', 'Hazelcrest', '84524');
insert into pickup_places (id, country, city, street, number)
values (226, 'Spain', 'Pontevedra', 'Novick', '256');
insert into pickup_places (id, country, city, street, number)
values (227, 'Japan', 'Suita', 'Onsgard', '36216');
insert into pickup_places (id, country, city, street, number)
values (228, 'China', 'Hekou', 'Fair Oaks', '647');
insert into pickup_places (id, country, city, street, number)
values (229, 'France', 'Limoges', 'Burrows', '16174');
insert into pickup_places (id, country, city, street, number)
values (230, 'Zambia', 'Chipata', 'Meadow Vale', '2735');
insert into pickup_places (id, country, city, street, number)
values (231, 'Poland', 'Kuczbork-Osada', 'Evergreen', '038');
insert into pickup_places (id, country, city, street, number)
values (232, 'Netherlands', 'IJmuiden', 'Mayfield', '21796');
insert into pickup_places (id, country, city, street, number)
values (233, 'Colombia', 'Tuluá', 'Anzinger', '6046');
insert into pickup_places (id, country, city, street, number)
values (234, 'Iran', 'Malekān', 'Morrow', '78358');
insert into pickup_places (id, country, city, street, number)
values (235, 'Albania', 'Baz', 'Donald', '27704');
insert into pickup_places (id, country, city, street, number)
values (236, 'Russia', 'Yaroslavl', 'Raven', '6457');
insert into pickup_places (id, country, city, street, number)
values (237, 'Russia', 'Yelan’-Kolenovskiy', 'Oneill', '393');
insert into pickup_places (id, country, city, street, number)
values (238, 'Greece', 'Kallithéa', 'Riverside', '821');
insert into pickup_places (id, country, city, street, number)
values (239, 'Peru', 'Caleta Cruz', 'Sachs', '918');
insert into pickup_places (id, country, city, street, number)
values (240, 'Norway', 'Oslo', 'Dexter', '0');
insert into pickup_places (id, country, city, street, number)
values (241, 'Sweden', 'Angered', 'Johnson', '84346');
insert into pickup_places (id, country, city, street, number)
values (242, 'Poland', 'Sulbiny Górne', 'Westport', '658');
insert into pickup_places (id, country, city, street, number)
values (243, 'Norway', 'Kristiansand S', 'Donald', '707');
insert into pickup_places (id, country, city, street, number)
values (244, 'China', 'Futian', 'Little Fleur', '1280');
insert into pickup_places (id, country, city, street, number)
values (245, 'France', 'Bordeaux', 'Harbort', '02193');
insert into pickup_places (id, country, city, street, number)
values (246, 'China', 'Daoxian', 'Transport', '91917');
insert into pickup_places (id, country, city, street, number)
values (247, 'Russia', 'Ust’-Labinsk', 'Cherokee', '900');
insert into pickup_places (id, country, city, street, number)
values (248, 'Somalia', 'Waajid', 'Blackbird', '4650');
insert into pickup_places (id, country, city, street, number)
values (249, 'Sri Lanka', 'Sigiriya', 'Tomscot', '88');
insert into pickup_places (id, country, city, street, number)
values (250, 'China', 'Shimeitang', 'Crest Line', '75780');
insert into pickup_places (id, country, city, street, number)
values (251, 'Indonesia', 'Krajan Alastengah', 'Macpherson', '3');
insert into pickup_places (id, country, city, street, number)
values (252, 'Brazil', 'Vilhena', 'Anderson', '242');
insert into pickup_places (id, country, city, street, number)
values (253, 'Czech Republic', 'Bystřany', 'Bay', '7');
insert into pickup_places (id, country, city, street, number)
values (254, 'China', 'Zaolin', 'Morningstar', '247');
insert into pickup_places (id, country, city, street, number)
values (255, 'China', 'Heiheba', 'Atwood', '1382');
insert into pickup_places (id, country, city, street, number)
values (256, 'Indonesia', 'Werinama', 'Parkside', '19208');
insert into pickup_places (id, country, city, street, number)
values (257, 'Indonesia', 'Krayen', 'Dryden', '5');
insert into pickup_places (id, country, city, street, number)
values (258, 'Indonesia', 'Ciketak', 'Menomonie', '3');
insert into pickup_places (id, country, city, street, number)
values (259, 'China', 'Hengling', 'Fulton', '284');
insert into pickup_places (id, country, city, street, number)
values (260, 'Japan', 'Ayabe', 'Dakota', '38');
insert into pickup_places (id, country, city, street, number)
values (261, 'Chad', 'Mboursou Léré', 'East', '9579');
insert into pickup_places (id, country, city, street, number)
values (262, 'China', 'Piaocao', 'Pawling', '1');
insert into pickup_places (id, country, city, street, number)
values (263, 'China', 'Yejia', 'International', '06954');
insert into pickup_places (id, country, city, street, number)
values (264, 'China', 'Hezuo', 'Green', '3');
insert into pickup_places (id, country, city, street, number)
values (265, 'Portugal', 'Portelinha', 'Kennedy', '33024');
insert into pickup_places (id, country, city, street, number)
values (266, 'China', 'Dagushan', '6th', '463');
insert into pickup_places (id, country, city, street, number)
values (267, 'Indonesia', 'Purwokerto', '2nd', '26');
insert into pickup_places (id, country, city, street, number)
values (268, 'Belarus', 'Karanyowka', 'Hoepker', '825');
insert into pickup_places (id, country, city, street, number)
values (269, 'Indonesia', 'Semboropasar', 'Hauk', '9');
insert into pickup_places (id, country, city, street, number)
values (270, 'Russia', 'Ozëry', 'Almo', '66');
insert into pickup_places (id, country, city, street, number)
values (271, 'Indonesia', 'Dadap', 'Butterfield', '9879');
insert into pickup_places (id, country, city, street, number)
values (272, 'Andorra', 'Sant Julià de Lòria', 'Mesta', '0052');
insert into pickup_places (id, country, city, street, number)
values (273, 'Canada', 'Smoky Lake', 'Sullivan', '2410');
insert into pickup_places (id, country, city, street, number)
values (274, 'Poland', 'Jonkowo', 'Crescent Oaks', '6029');
insert into pickup_places (id, country, city, street, number)
values (275, 'Vietnam', 'Quận Sáu', 'Norway Maple', '72');
insert into pickup_places (id, country, city, street, number)
values (276, 'China', 'Chaoyang', 'Oakridge', '7');
insert into pickup_places (id, country, city, street, number)
values (277, 'Colombia', 'Ciénaga', 'Weeping Birch', '75');
insert into pickup_places (id, country, city, street, number)
values (278, 'Uruguay', 'Santa Catalina', 'Schiller', '19');
insert into pickup_places (id, country, city, street, number)
values (279, 'Peru', 'Ilo', 'Londonderry', '873');
insert into pickup_places (id, country, city, street, number)
values (280, 'China', 'Piduhe', 'Dunning', '3');
insert into pickup_places (id, country, city, street, number)
values (281, 'Thailand', 'Pong', 'Esker', '07817');
insert into pickup_places (id, country, city, street, number)
values (282, 'Sweden', 'Sandviken', 'Sherman', '27');
insert into pickup_places (id, country, city, street, number)
values (283, 'Venezuela', 'Yaguaraparo', 'Delaware', '06');
insert into pickup_places (id, country, city, street, number)
values (284, 'China', 'Shaoxing', 'Bunting', '9');
insert into pickup_places (id, country, city, street, number)
values (285, 'Russia', 'Novo-Nikol’skoye', 'Lotheville', '79');
insert into pickup_places (id, country, city, street, number)
values (286, 'Nigeria', 'Danja', 'Forest Run', '8345');
insert into pickup_places (id, country, city, street, number)
values (287, 'Peru', 'Huanza', 'Doe Crossing', '24');
insert into pickup_places (id, country, city, street, number)
values (288, 'Russia', 'Pyatovskiy', 'Carpenter', '16');
insert into pickup_places (id, country, city, street, number)
values (289, 'Brazil', 'Jutaí', 'Mallard', '6');
insert into pickup_places (id, country, city, street, number)
values (290, 'Indonesia', 'Salam', 'Del Sol', '597');
insert into pickup_places (id, country, city, street, number)
values (291, 'Nigeria', 'Ode', 'Clemons', '60060');
insert into pickup_places (id, country, city, street, number)
values (292, 'China', 'Zhouling', 'Lillian', '243');
insert into pickup_places (id, country, city, street, number)
values (293, 'Philippines', 'Paldit', 'Forster', '41728');
insert into pickup_places (id, country, city, street, number)
values (294, 'China', 'Tanbu', 'Onsgard', '104');
insert into pickup_places (id, country, city, street, number)
values (295, 'China', 'Xishanzui', '4th', '993');
insert into pickup_places (id, country, city, street, number)
values (296, 'China', 'Xinmin', 'Basil', '0181');
insert into pickup_places (id, country, city, street, number)
values (297, 'Philippines', 'Santo Niño', 'Clemons', '68');
insert into pickup_places (id, country, city, street, number)
values (298, 'Indonesia', 'Kalidung', 'Dakota', '267');
insert into pickup_places (id, country, city, street, number)
values (299, 'Syria', 'Jablah', 'Express', '80');
insert into pickup_places (id, country, city, street, number)
values (300, 'Indonesia', 'Klatakan', 'Upham', '830');
insert into pickup_places (id, country, city, street, number)
values (301, 'Philippines', 'Monamon', 'Russell', '04636');
insert into pickup_places (id, country, city, street, number)
values (302, 'Indonesia', 'Wadung', 'Ludington', '5');
insert into pickup_places (id, country, city, street, number)
values (303, 'Canada', 'Gananoque', 'Ludington', '8');
insert into pickup_places (id, country, city, street, number)
values (304, 'Portugal', 'Granja', 'Lerdahl', '925');
insert into pickup_places (id, country, city, street, number)
values (305, 'Vietnam', 'Thành Phố Nam Định', 'Karstens', '156');
insert into pickup_places (id, country, city, street, number)
values (306, 'China', 'Wangzuizi', 'High Crossing', '00');
insert into pickup_places (id, country, city, street, number)
values (307, 'China', 'Hanghuadian', 'Portage', '29');
insert into pickup_places (id, country, city, street, number)
values (308, 'Poland', 'Białobrzegi', 'Rutledge', '6247');
insert into pickup_places (id, country, city, street, number)
values (309, 'China', 'Xunhe', 'Straubel', '10259');
insert into pickup_places (id, country, city, street, number)
values (310, 'Philippines', 'San Quintin', 'Crownhardt', '0304');
insert into pickup_places (id, country, city, street, number)
values (311, 'Ukraine', 'Dykan’ka', 'Butternut', '162');
insert into pickup_places (id, country, city, street, number)
values (312, 'Iran', 'Noşratābād', 'Jenifer', '28769');
insert into pickup_places (id, country, city, street, number)
values (313, 'Sweden', 'Stockholm', 'Forster', '038');
insert into pickup_places (id, country, city, street, number)
values (314, 'China', 'Liuhu', 'Village Green', '39777');
insert into pickup_places (id, country, city, street, number)
values (315, 'Dominican Republic', 'San José de Ocoa', 'Valley Edge', '18');
insert into pickup_places (id, country, city, street, number)
values (316, 'China', 'Jiuhua', 'Weeping Birch', '74');
insert into pickup_places (id, country, city, street, number)
values (317, 'Russia', 'Turochak', 'Longview', '2653');
insert into pickup_places (id, country, city, street, number)
values (318, 'China', 'Yayao', 'Village', '57');
insert into pickup_places (id, country, city, street, number)
values (319, 'Indonesia', 'Kalumpang', 'Petterle', '37343');
insert into pickup_places (id, country, city, street, number)
values (320, 'Taiwan', 'Miaoli', 'Burning Wood', '37354');
insert into pickup_places (id, country, city, street, number)
values (321, 'Ireland', 'Kinsealy-Drinan', 'Cardinal', '36343');
insert into pickup_places (id, country, city, street, number)
values (322, 'Uzbekistan', 'Manghit', 'Schlimgen', '531');
insert into pickup_places (id, country, city, street, number)
values (323, 'Syria', 'Armanāz', 'Debs', '52');
insert into pickup_places (id, country, city, street, number)
values (324, 'China', 'Altunemil', 'Donald', '2');
insert into pickup_places (id, country, city, street, number)
values (325, 'Nigeria', 'Orlu', 'Lighthouse Bay', '4');
insert into pickup_places (id, country, city, street, number)
values (326, 'Brazil', 'Vargem Grande', 'Thompson', '9');
insert into pickup_places (id, country, city, street, number)
values (327, 'Kazakhstan', 'Shchūchīnsk', 'Claremont', '106');
insert into pickup_places (id, country, city, street, number)
values (328, 'China', 'Zhanghuban', 'Mitchell', '56');
insert into pickup_places (id, country, city, street, number)
values (329, 'Vietnam', 'Thạnh Mỹ', 'Pennsylvania', '0');
insert into pickup_places (id, country, city, street, number)
values (330, 'China', 'Dietaisi', 'Hermina', '0');
insert into pickup_places (id, country, city, street, number)
values (331, 'Botswana', 'Gaborone', 'Loomis', '39474');
insert into pickup_places (id, country, city, street, number)
values (332, 'Poland', 'Chełm', 'Moulton', '01');
insert into pickup_places (id, country, city, street, number)
values (333, 'Finland', 'Taivalkoski', 'Everett', '1131');
insert into pickup_places (id, country, city, street, number)
values (334, 'Philippines', 'Baliuag Nuevo', 'Maryland', '3');
insert into pickup_places (id, country, city, street, number)
values (335, 'Pakistan', 'Thul', 'Forster', '76');
insert into pickup_places (id, country, city, street, number)
values (336, 'Portugal', 'Casais', 'Toban', '343');
insert into pickup_places (id, country, city, street, number)
values (337, 'Argentina', 'La Francia', 'Claremont', '5');
insert into pickup_places (id, country, city, street, number)
values (338, 'China', 'Wudangshan', 'Crescent Oaks', '53');
insert into pickup_places (id, country, city, street, number)
values (339, 'China', 'Maogang', 'Anhalt', '9107');
insert into pickup_places (id, country, city, street, number)
values (340, 'Philippines', 'Masaling', 'Mitchell', '56456');
insert into pickup_places (id, country, city, street, number)
values (341, 'Georgia', 'Akhaltsikhe', 'Mallard', '68');
insert into pickup_places (id, country, city, street, number)
values (342, 'Brazil', 'Patrocínio', '7th', '619');
insert into pickup_places (id, country, city, street, number)
values (343, 'Brazil', 'Santa Rosa', 'Kipling', '44216');
insert into pickup_places (id, country, city, street, number)
values (344, 'Russia', 'Nikolayevsk', 'Thompson', '6');
insert into pickup_places (id, country, city, street, number)
values (345, 'China', 'Jiujie', 'Judy', '45253');
insert into pickup_places (id, country, city, street, number)
values (346, 'Panama', 'Alto de la Estancia', 'Brown', '6');
insert into pickup_places (id, country, city, street, number)
values (347, 'United Arab Emirates', 'Ar Ruways', 'Mcguire', '5');
insert into pickup_places (id, country, city, street, number)
values (348, 'Philippines', 'Llanera', 'Lakeland', '40');
insert into pickup_places (id, country, city, street, number)
values (349, 'Indonesia', 'Talondang', 'Ronald Regan', '28592');
insert into pickup_places (id, country, city, street, number)
values (350, 'France', 'Agen', 'Fremont', '70507');
insert into pickup_places (id, country, city, street, number)
values (351, 'Dominican Republic', 'Santo Domingo', 'Schiller', '157');
insert into pickup_places (id, country, city, street, number)
values (352, 'Tunisia', 'Hammam Sousse', 'Mendota', '62');
insert into pickup_places (id, country, city, street, number)
values (353, 'China', 'Zhongguan', 'Dawn', '63684');
insert into pickup_places (id, country, city, street, number)
values (354, 'Tanzania', 'Ngorongoro', 'Duke', '09915');
insert into pickup_places (id, country, city, street, number)
values (355, 'Mexico', 'Santa Elena', 'Esker', '50306');
insert into pickup_places (id, country, city, street, number)
values (356, 'Indonesia', 'Coratatal', 'Jenifer', '01354');
insert into pickup_places (id, country, city, street, number)
values (357, 'China', 'Songjianghe', 'Hovde', '17061');
insert into pickup_places (id, country, city, street, number)
values (358, 'Jordan', 'Zarqa', 'Cambridge', '630');
insert into pickup_places (id, country, city, street, number)
values (359, 'Poland', 'Czarnowąsy', 'Fieldstone', '9003');
insert into pickup_places (id, country, city, street, number)
values (360, 'China', 'Yaozhuang', 'Talmadge', '13');
insert into pickup_places (id, country, city, street, number)
values (361, 'Philippines', 'Manay', 'Dennis', '368');
insert into pickup_places (id, country, city, street, number)
values (362, 'Colombia', 'Maicao', 'Jackson', '8115');
insert into pickup_places (id, country, city, street, number)
values (363, 'Jamaica', 'Linstead', 'Buena Vista', '0');
insert into pickup_places (id, country, city, street, number)
values (364, 'China', 'Xarsingma', 'Lunder', '067');
insert into pickup_places (id, country, city, street, number)
values (365, 'Philippines', 'Sagana', 'Nelson', '55347');
insert into pickup_places (id, country, city, street, number)
values (366, 'Philippines', 'Mondragon', 'Schurz', '5');
insert into pickup_places (id, country, city, street, number)
values (367, 'Peru', 'Uchiza', 'Maryland', '71');
insert into pickup_places (id, country, city, street, number)
values (368, 'Slovenia', 'Ljubno ob Savinji', 'Walton', '450');
insert into pickup_places (id, country, city, street, number)
values (369, 'Indonesia', 'Kuafeu', 'Green Ridge', '8');
insert into pickup_places (id, country, city, street, number)
values (370, 'China', 'Dongyu', 'Maple', '7');
insert into pickup_places (id, country, city, street, number)
values (371, 'Indonesia', 'Terang', 'John Wall', '61539');
insert into pickup_places (id, country, city, street, number)
values (372, 'Philippines', 'Pontian', 'Artisan', '0214');
insert into pickup_places (id, country, city, street, number)
values (373, 'France', 'Courtaboeuf', 'Dakota', '1059');
insert into pickup_places (id, country, city, street, number)
values (374, 'Brazil', 'Cajueiro', 'Sage', '06986');
insert into pickup_places (id, country, city, street, number)
values (375, 'Japan', 'Ageoshimo', 'Talmadge', '68');
insert into pickup_places (id, country, city, street, number)
values (376, 'Japan', 'Imari', 'Hooker', '9');
insert into pickup_places (id, country, city, street, number)
values (377, 'France', 'Villejuif', 'Morningstar', '62');
insert into pickup_places (id, country, city, street, number)
values (378, 'Russia', 'Nevinnomyssk', 'Mandrake', '75');
insert into pickup_places (id, country, city, street, number)
values (379, 'Poland', 'Leśna Podlaska', 'Petterle', '24482');
insert into pickup_places (id, country, city, street, number)
values (380, 'Philippines', 'Dimataling', 'Fordem', '78');
insert into pickup_places (id, country, city, street, number)
values (381, 'Sweden', 'Västerås', 'Hintze', '7');
insert into pickup_places (id, country, city, street, number)
values (382, 'Ukraine', 'Lisovi Sorochyntsi', 'Southridge', '16825');
insert into pickup_places (id, country, city, street, number)
values (383, 'Poland', 'Wydminy', 'Coleman', '399');
insert into pickup_places (id, country, city, street, number)
values (384, 'Indonesia', 'Rancaranji', 'Lunder', '94161');
insert into pickup_places (id, country, city, street, number)
values (385, 'Indonesia', 'Seupakat', 'Maple', '91');
insert into pickup_places (id, country, city, street, number)
values (386, 'Mongolia', 'Öndörhoshuu', 'Northridge', '0');
insert into pickup_places (id, country, city, street, number)
values (387, 'Thailand', 'Chanthaburi', 'La Follette', '2');
insert into pickup_places (id, country, city, street, number)
values (388, 'India', 'Sadar Bazar', 'La Follette', '725');
insert into pickup_places (id, country, city, street, number)
values (389, 'Luxembourg', 'Leudelange', 'Comanche', '81165');
insert into pickup_places (id, country, city, street, number)
values (390, 'Indonesia', 'Merapit', 'American', '3');
insert into pickup_places (id, country, city, street, number)
values (391, 'Greece', 'Pérama', 'Lerdahl', '7');
insert into pickup_places (id, country, city, street, number)
values (392, 'China', 'Zhaolingpu', 'Waubesa', '517');
insert into pickup_places (id, country, city, street, number)
values (393, 'Mauritania', 'Aleg', 'Lillian', '45142');
insert into pickup_places (id, country, city, street, number)
values (394, 'Norway', 'Bergen', 'Schmedeman', '51');
insert into pickup_places (id, country, city, street, number)
values (395, 'Portugal', 'Companhia de Baixo', 'Corscot', '3');
insert into pickup_places (id, country, city, street, number)
values (396, 'China', 'Pingdingshan', 'Green Ridge', '7457');
insert into pickup_places (id, country, city, street, number)
values (397, 'United States', 'Memphis', 'Vermont', '3724');
insert into pickup_places (id, country, city, street, number)
values (398, 'Indonesia', 'Ronggo', 'Mallard', '8');
insert into pickup_places (id, country, city, street, number)
values (399, 'Kyrgyzstan', 'Kaindy', 'Sunnyside', '64469');
insert into pickup_places (id, country, city, street, number)
values (400, 'Indonesia', 'Karangasem', 'Lighthouse Bay', '288');
insert into pickup_places (id, country, city, street, number)
values (401, 'Honduras', 'Baja Mar', 'John Wall', '88386');
insert into pickup_places (id, country, city, street, number)
values (402, 'Bulgaria', 'Kiten', 'American', '002');
insert into pickup_places (id, country, city, street, number)
values (403, 'South Korea', 'Jeju-si', 'Pawling', '6');
insert into pickup_places (id, country, city, street, number)
values (404, 'Croatia', 'Vodnjan', 'Lyons', '94231');
insert into pickup_places (id, country, city, street, number)
values (405, 'Philippines', 'San Juan de Mata', 'Dexter', '96405');
insert into pickup_places (id, country, city, street, number)
values (406, 'United States', 'Norfolk', 'Park Meadow', '3454');
insert into pickup_places (id, country, city, street, number)
values (407, 'China', 'Nan’an', 'Valley Edge', '54');
insert into pickup_places (id, country, city, street, number)
values (408, 'China', 'Wuli', 'Blackbird', '3408');
insert into pickup_places (id, country, city, street, number)
values (409, 'Indonesia', 'Perreng', 'Riverside', '30303');
insert into pickup_places (id, country, city, street, number)
values (410, 'France', 'Moulins', 'Westerfield', '90');
insert into pickup_places (id, country, city, street, number)
values (411, 'Morocco', 'Boujniba', 'Canary', '9');
insert into pickup_places (id, country, city, street, number)
values (412, 'Malawi', 'Mchinji', 'Cascade', '637');
insert into pickup_places (id, country, city, street, number)
values (413, 'Philippines', 'Tangub', 'Prentice', '39');
insert into pickup_places (id, country, city, street, number)
values (414, 'Indonesia', 'Desa Wetan Ciakar', 'Blue Bill Park', '23085');
insert into pickup_places (id, country, city, street, number)
values (415, 'China', 'Baohe', 'Dahle', '458');
insert into pickup_places (id, country, city, street, number)
values (416, 'Malaysia', 'Bandar Pusat Jengka', 'Moland', '7');
insert into pickup_places (id, country, city, street, number)
values (417, 'Equatorial Guinea', 'Ncue', 'Schlimgen', '91');
insert into pickup_places (id, country, city, street, number)
values (418, 'China', 'Khuma', 'Mockingbird', '89');
insert into pickup_places (id, country, city, street, number)
values (419, 'Indonesia', 'Cimrutu', 'Anderson', '2103');
insert into pickup_places (id, country, city, street, number)
values (420, 'China', 'Cizhu', 'Drewry', '64243');
insert into pickup_places (id, country, city, street, number)
values (421, 'Vietnam', 'Trà Vinh', 'Golf', '75');
insert into pickup_places (id, country, city, street, number)
values (422, 'Georgia', 'Shorapani', 'Jackson', '24543');
insert into pickup_places (id, country, city, street, number)
values (423, 'Czech Republic', 'Častolovice', 'Roxbury', '61');
insert into pickup_places (id, country, city, street, number)
values (424, 'Ireland', 'Virginia', 'Westend', '95');
insert into pickup_places (id, country, city, street, number)
values (425, 'France', 'Sartrouville', 'Buell', '8');
insert into pickup_places (id, country, city, street, number)
values (426, 'France', 'Lyon', 'Surrey', '850');
insert into pickup_places (id, country, city, street, number)
values (427, 'Yemen', 'Mabar', 'Jackson', '33709');
insert into pickup_places (id, country, city, street, number)
values (428, 'France', 'Mont-de-Marsan', 'Comanche', '9');
insert into pickup_places (id, country, city, street, number)
values (429, 'Vietnam', 'Cái Dầu', 'Mitchell', '07140');
insert into pickup_places (id, country, city, street, number)
values (430, 'China', 'Huaqiao', 'Anderson', '34504');
insert into pickup_places (id, country, city, street, number)
values (431, 'Lithuania', 'Priekulė', 'Debs', '6');
insert into pickup_places (id, country, city, street, number)
values (432, 'Mexico', 'Pemex', 'Katie', '45192');
insert into pickup_places (id, country, city, street, number)
values (433, 'Russia', 'Novosmolinskiy', 'Superior', '17');
insert into pickup_places (id, country, city, street, number)
values (434, 'China', 'Dongkan', 'Debra', '8');
insert into pickup_places (id, country, city, street, number)
values (435, 'Russia', 'Pokrovskoye', 'Westend', '4');
insert into pickup_places (id, country, city, street, number)
values (436, 'Indonesia', 'Kademangan', 'Victoria', '3');
insert into pickup_places (id, country, city, street, number)
values (437, 'Colombia', 'El Bagre', 'Gale', '31811');
insert into pickup_places (id, country, city, street, number)
values (438, 'Canada', 'Saint-Lambert-de-Lauzon', 'Twin Pines', '4025');
insert into pickup_places (id, country, city, street, number)
values (439, 'China', 'Heshan', 'Rigney', '88');
insert into pickup_places (id, country, city, street, number)
values (440, 'China', 'Huazhou', 'Mifflin', '6823');
insert into pickup_places (id, country, city, street, number)
values (441, 'Indonesia', 'Selaawi', 'Beilfuss', '2453');
insert into pickup_places (id, country, city, street, number)
values (442, 'Brazil', 'Promissão', 'Farwell', '14');
insert into pickup_places (id, country, city, street, number)
values (443, 'Philippines', 'Rizal', 'Prairieview', '535');
insert into pickup_places (id, country, city, street, number)
values (444, 'China', 'Xinsheng', 'Marquette', '3');
insert into pickup_places (id, country, city, street, number)
values (445, 'China', 'Machikou', 'Drewry', '15360');
insert into pickup_places (id, country, city, street, number)
values (446, 'Mexico', 'El Zapote', 'Oriole', '0');
insert into pickup_places (id, country, city, street, number)
values (447, 'Indonesia', 'Karangsari', 'Cascade', '9778');
insert into pickup_places (id, country, city, street, number)
values (448, 'Russia', 'Divnomorskoye', 'Beilfuss', '10740');
insert into pickup_places (id, country, city, street, number)
values (449, 'Philippines', 'San Emilio', 'Barnett', '699');
insert into pickup_places (id, country, city, street, number)
values (450, 'El Salvador', 'Santiago Nonualco', 'Northland', '36787');
insert into pickup_places (id, country, city, street, number)
values (451, 'Poland', 'Koronowo', 'Arkansas', '682');
insert into pickup_places (id, country, city, street, number)
values (452, 'Japan', 'Okayama-shi', 'Warner', '054');
insert into pickup_places (id, country, city, street, number)
values (453, 'Russia', 'Rzhavki', 'Oriole', '4565');
insert into pickup_places (id, country, city, street, number)
values (454, 'Azerbaijan', 'Quba', 'Northland', '84133');
insert into pickup_places (id, country, city, street, number)
values (455, 'Ukraine', 'Stavyshche', 'Fulton', '87666');
insert into pickup_places (id, country, city, street, number)
values (456, 'China', 'Sidu', 'Hanover', '0971');
insert into pickup_places (id, country, city, street, number)
values (457, 'Peru', 'Ullulluco', 'Lotheville', '79461');
insert into pickup_places (id, country, city, street, number)
values (458, 'Sweden', 'Österbybruk', 'Eagan', '05156');
insert into pickup_places (id, country, city, street, number)
values (459, 'Indonesia', 'Sadar', 'Sunbrook', '5152');
insert into pickup_places (id, country, city, street, number)
values (460, 'Greece', 'Vrilissia', 'Welch', '5814');
insert into pickup_places (id, country, city, street, number)
values (461, 'Ukraine', 'Konotop', 'Thompson', '3883');
insert into pickup_places (id, country, city, street, number)
values (462, 'China', 'Xiasha', 'Oxford', '258');
insert into pickup_places (id, country, city, street, number)
values (463, 'Philippines', 'Tamnag', 'Fuller', '65523');
insert into pickup_places (id, country, city, street, number)
values (464, 'Peru', 'Changuillo', 'Doe Crossing', '136');
insert into pickup_places (id, country, city, street, number)
values (465, 'Hungary', 'Budapest', 'Forest Dale', '90');
insert into pickup_places (id, country, city, street, number)
values (466, 'Pakistan', 'Hujra', 'Bayside', '2');
insert into pickup_places (id, country, city, street, number)
values (467, 'Portugal', 'Guia', 'Garrison', '6887');
insert into pickup_places (id, country, city, street, number)
values (468, 'China', 'Huaping', 'Kropf', '1343');
insert into pickup_places (id, country, city, street, number)
values (469, 'Philippines', 'Can-Avid', 'Rieder', '12702');
insert into pickup_places (id, country, city, street, number)
values (470, 'France', 'Vitry-sur-Seine', 'Starling', '96586');
insert into pickup_places (id, country, city, street, number)
values (471, 'Sudan', 'Wagar', 'Clarendon', '550');
insert into pickup_places (id, country, city, street, number)
values (472, 'Russia', 'Kosh-Agach', 'Division', '6240');
insert into pickup_places (id, country, city, street, number)
values (473, 'Dominican Republic', 'Bayaguana', 'Grasskamp', '16');
insert into pickup_places (id, country, city, street, number)
values (474, 'Czech Republic', 'Chřibská', 'Mendota', '65');
insert into pickup_places (id, country, city, street, number)
values (475, 'Japan', 'Satsumasendai', 'Doe Crossing', '18552');
insert into pickup_places (id, country, city, street, number)
values (476, 'Saudi Arabia', 'Hayil', 'Merry', '9');
insert into pickup_places (id, country, city, street, number)
values (477, 'Bolivia', 'San Ignacio de Velasco', 'Prairie Rose', '2');
insert into pickup_places (id, country, city, street, number)
values (478, 'Poland', 'Jutrosin', 'Mariners Cove', '808');
insert into pickup_places (id, country, city, street, number)
values (479, 'Bosnia and Herzegovina', 'Kladanj', 'Nelson', '7');
insert into pickup_places (id, country, city, street, number)
values (480, 'Peru', 'Tomay Kichwa', 'Steensland', '140');
insert into pickup_places (id, country, city, street, number)
values (481, 'Nigeria', 'Magumeri', 'Park Meadow', '10393');
insert into pickup_places (id, country, city, street, number)
values (482, 'Russia', 'Krasnofarfornyy', 'Packers', '46487');
insert into pickup_places (id, country, city, street, number)
values (483, 'Puerto Rico', 'Carolina', 'International', '12962');
insert into pickup_places (id, country, city, street, number)
values (484, 'Ukraine', 'Bilshivtsi', 'Independence', '233');
insert into pickup_places (id, country, city, street, number)
values (485, 'Botswana', 'Letlhakane', 'Sloan', '505');
insert into pickup_places (id, country, city, street, number)
values (486, 'France', 'Mulhouse', 'Havey', '21092');
insert into pickup_places (id, country, city, street, number)
values (487, 'Brazil', 'Bom Despacho', 'Division', '1');
insert into pickup_places (id, country, city, street, number)
values (488, 'Russia', 'Novovladykino', 'David', '1');
insert into pickup_places (id, country, city, street, number)
values (489, 'Morocco', 'Targuist', 'East', '54');
insert into pickup_places (id, country, city, street, number)
values (490, 'Poland', 'Reda', 'Debs', '5322');
insert into pickup_places (id, country, city, street, number)
values (491, 'South Korea', 'Andong', 'Melody', '1718');
insert into pickup_places (id, country, city, street, number)
values (492, 'Philippines', 'Cabaritan East', 'Londonderry', '59585');
insert into pickup_places (id, country, city, street, number)
values (493, 'Pakistan', 'Tando Muhammad Khān', 'Fremont', '407');
insert into pickup_places (id, country, city, street, number)
values (494, 'China', 'Sigeng', 'Schlimgen', '4391');
insert into pickup_places (id, country, city, street, number)
values (495, 'China', 'Gangkoujie', 'Maple Wood', '69300');
insert into pickup_places (id, country, city, street, number)
values (496, 'Russia', 'Novoye Leushino', 'Shopko', '12');
insert into pickup_places (id, country, city, street, number)
values (497, 'China', 'Sankeyushu', 'Elka', '235');
insert into pickup_places (id, country, city, street, number)
values (498, 'Indonesia', 'Tubuhue', 'Birchwood', '669');
insert into pickup_places (id, country, city, street, number)
values (499, 'Mongolia', 'Tögrög', 'Portage', '61534');
insert into pickup_places (id, country, city, street, number)
values (500, 'Peru', 'Aco', 'Dottie', '80');
insert into pickup_places (id, country, city, street, number)
values (501, 'Japan', 'Tosu', 'Service', '1');
insert into pickup_places (id, country, city, street, number)
values (502, 'China', 'Xinle', 'Bonner', '114');
insert into pickup_places (id, country, city, street, number)
values (503, 'Philippines', 'Sonquil', 'Cambridge', '5');
insert into pickup_places (id, country, city, street, number)
values (504, 'Philippines', 'Guruyan', 'Burning Wood', '73223');
insert into pickup_places (id, country, city, street, number)
values (505, 'Russia', 'Nerekhta', 'Loeprich', '614');
insert into pickup_places (id, country, city, street, number)
values (506, 'Macedonia', 'Bosilovo', 'Graceland', '4751');
insert into pickup_places (id, country, city, street, number)
values (507, 'Philippines', 'Tignapalan', 'Crescent Oaks', '958');
insert into pickup_places (id, country, city, street, number)
values (508, 'Spain', 'Palmas De Gran Canaria, Las', 'Emmet', '28108');
insert into pickup_places (id, country, city, street, number)
values (509, 'Denmark', 'København', 'Fuller', '327');
insert into pickup_places (id, country, city, street, number)
values (510, 'Costa Rica', 'Cartago', 'Anhalt', '0229');
insert into pickup_places (id, country, city, street, number)
values (511, 'Peru', 'San Buenaventura', 'Bowman', '658');
insert into pickup_places (id, country, city, street, number)
values (512, 'Russia', 'Lesnyye Polyany', 'Alpine', '15534');
insert into pickup_places (id, country, city, street, number)
values (513, 'Indonesia', 'Krikil', 'Marquette', '061');
insert into pickup_places (id, country, city, street, number)
values (514, 'China', 'Shataping', 'Warbler', '246');
insert into pickup_places (id, country, city, street, number)
values (515, 'Indonesia', 'Gendiwu', 'Kingsford', '83');
insert into pickup_places (id, country, city, street, number)
values (516, 'China', 'Liuduzhai', 'Warner', '0327');
insert into pickup_places (id, country, city, street, number)
values (517, 'Sweden', 'Vaxholm', 'Lotheville', '435');
insert into pickup_places (id, country, city, street, number)
values (518, 'Philippines', 'Luzon', 'School', '7002');
insert into pickup_places (id, country, city, street, number)
values (519, 'Philippines', 'Veruela', 'Linden', '263');
insert into pickup_places (id, country, city, street, number)
values (520, 'China', 'Hengshui', 'Derek', '7385');
insert into pickup_places (id, country, city, street, number)
values (521, 'Japan', 'Wakimachi', 'Barnett', '917');
insert into pickup_places (id, country, city, street, number)
values (522, 'Indonesia', 'Ciangir', 'Meadow Vale', '017');
insert into pickup_places (id, country, city, street, number)
values (523, 'Colombia', 'Sahagún', 'Cottonwood', '8');
insert into pickup_places (id, country, city, street, number)
values (524, 'France', 'Nice', 'Dennis', '77405');
insert into pickup_places (id, country, city, street, number)
values (525, 'Ukraine', 'Horokhiv', 'Banding', '027');
insert into pickup_places (id, country, city, street, number)
values (526, 'Guam', 'Santa Rita Village', 'Roxbury', '31');
insert into pickup_places (id, country, city, street, number)
values (527, 'Malaysia', 'Pulau Pinang', 'Arkansas', '57');
insert into pickup_places (id, country, city, street, number)
values (528, 'Poland', 'Brzeszcze', 'Ridgeview', '929');
insert into pickup_places (id, country, city, street, number)
values (529, 'Indonesia', 'Gurung', 'Farragut', '70');
insert into pickup_places (id, country, city, street, number)
values (530, 'Philippines', 'Cama Juan', 'Russell', '55');
insert into pickup_places (id, country, city, street, number)
values (531, 'France', 'Paris 08', 'Oak', '36375');
insert into pickup_places (id, country, city, street, number)
values (532, 'China', 'Zhanghua', 'Ohio', '03');
insert into pickup_places (id, country, city, street, number)
values (533, 'Indonesia', 'Watulumbung', 'Bultman', '18618');
insert into pickup_places (id, country, city, street, number)
values (534, 'Czech Republic', 'Stará Huť', 'Amoth', '4');
insert into pickup_places (id, country, city, street, number)
values (535, 'Thailand', 'Chiang Mai', 'Vahlen', '87');
insert into pickup_places (id, country, city, street, number)
values (536, 'Philippines', 'Sapang Buho', 'Main', '89');
insert into pickup_places (id, country, city, street, number)
values (537, 'Indonesia', 'Timur', 'Bobwhite', '3');
insert into pickup_places (id, country, city, street, number)
values (538, 'China', 'Dayangqi', 'Merry', '930');
insert into pickup_places (id, country, city, street, number)
values (539, 'China', 'Qinlan', 'Wayridge', '16839');
insert into pickup_places (id, country, city, street, number)
values (540, 'Russia', 'Ramenki', 'Rieder', '1323');
insert into pickup_places (id, country, city, street, number)
values (541, 'Portugal', 'Corga', 'Everett', '942');
insert into pickup_places (id, country, city, street, number)
values (542, 'China', 'Junbu', 'Dwight', '810');
insert into pickup_places (id, country, city, street, number)
values (543, 'Zambia', 'Lundazi', 'Milwaukee', '951');
insert into pickup_places (id, country, city, street, number)
values (544, 'Brazil', 'Águas Belas', 'Badeau', '722');
insert into pickup_places (id, country, city, street, number)
values (545, 'Guatemala', 'Almolonga', 'Anderson', '0');
insert into pickup_places (id, country, city, street, number)
values (546, 'China', 'Hongguang', 'Bay', '9675');
insert into pickup_places (id, country, city, street, number)
values (547, 'Philippines', 'Indulang', 'Michigan', '787');
insert into pickup_places (id, country, city, street, number)
values (548, 'China', 'Jieshou', 'Heath', '00');
insert into pickup_places (id, country, city, street, number)
values (549, 'Philippines', 'Panan', 'Holmberg', '205');
insert into pickup_places (id, country, city, street, number)
values (550, 'Indonesia', 'Karangcombong', 'Heath', '26906');
insert into pickup_places (id, country, city, street, number)
values (551, 'Brazil', 'Cabreúva', 'Shopko', '7');
insert into pickup_places (id, country, city, street, number)
values (552, 'Russia', 'Severnyy', 'Northland', '55');
insert into pickup_places (id, country, city, street, number)
values (553, 'Hungary', 'Békéscsaba', 'Hermina', '36');
insert into pickup_places (id, country, city, street, number)
values (554, 'Indonesia', 'Jetak', 'Carberry', '1');
insert into pickup_places (id, country, city, street, number)
values (555, 'Greece', 'Rizómilos', 'Milwaukee', '4744');
insert into pickup_places (id, country, city, street, number)
values (556, 'Peru', 'Lincha', 'Dottie', '05');
insert into pickup_places (id, country, city, street, number)
values (557, 'Russia', 'Ostashkov', 'Bluestem', '7642');
insert into pickup_places (id, country, city, street, number)
values (558, 'United States', 'New York City', 'Fulton', '89128');
insert into pickup_places (id, country, city, street, number)
values (559, 'Bosnia and Herzegovina', 'Brka', 'Delaware', '85900');
insert into pickup_places (id, country, city, street, number)
values (560, 'Portugal', 'Torres Novas', 'Golf View', '4367');
insert into pickup_places (id, country, city, street, number)
values (561, 'Yemen', 'Ḩajjah', 'Namekagon', '76950');
insert into pickup_places (id, country, city, street, number)
values (562, 'Thailand', 'Ban Rangsit', 'Truax', '6204');
insert into pickup_places (id, country, city, street, number)
values (563, 'Indonesia', 'Krajan', 'Loomis', '12');
insert into pickup_places (id, country, city, street, number)
values (564, 'China', 'Lianzhu', 'Katie', '6');
insert into pickup_places (id, country, city, street, number)
values (565, 'Philippines', 'Mercedes', 'Commercial', '9209');
insert into pickup_places (id, country, city, street, number)
values (566, 'Syria', 'Al Manşūrah', 'Bay', '9178');
insert into pickup_places (id, country, city, street, number)
values (567, 'Madagascar', 'Amparafaravola', 'Forest Dale', '37');
insert into pickup_places (id, country, city, street, number)
values (568, 'China', 'Sandaohezi', 'Tony', '49');
insert into pickup_places (id, country, city, street, number)
values (569, 'China', 'Yutou', 'Rusk', '07');
insert into pickup_places (id, country, city, street, number)
values (570, 'Indonesia', 'Sukadana', 'Kennedy', '8933');
insert into pickup_places (id, country, city, street, number)
values (571, 'Afghanistan', 'Qalah-ye Shai', 'Drewry', '2294');
insert into pickup_places (id, country, city, street, number)
values (572, 'Indonesia', 'Sinjai', 'Bunting', '6707');
insert into pickup_places (id, country, city, street, number)
values (573, 'China', 'Liubo', 'Jay', '4140');
insert into pickup_places (id, country, city, street, number)
values (574, 'Poland', 'Żabnica', 'Gateway', '900');
insert into pickup_places (id, country, city, street, number)
values (575, 'Malaysia', 'Kuala Lumpur', 'Texas', '2');
insert into pickup_places (id, country, city, street, number)
values (576, 'Thailand', 'Santi Suk', 'Stoughton', '3926');
insert into pickup_places (id, country, city, street, number)
values (577, 'South Korea', 'Icheon-si', 'Mallard', '651');
insert into pickup_places (id, country, city, street, number)
values (578, 'Thailand', 'Wiang Nuea', 'Hoepker', '41');
insert into pickup_places (id, country, city, street, number)
values (579, 'Ivory Coast', 'Abidjan', 'Lighthouse Bay', '90');
insert into pickup_places (id, country, city, street, number)
values (580, 'Croatia', 'Gospić', 'Bartelt', '4617');
insert into pickup_places (id, country, city, street, number)
values (581, 'Fiji', 'Nadi', 'Sauthoff', '7');
insert into pickup_places (id, country, city, street, number)
values (582, 'Indonesia', 'Cikaung', 'Hooker', '083');
insert into pickup_places (id, country, city, street, number)
values (583, 'Brazil', 'Araxá', 'Hallows', '41081');
insert into pickup_places (id, country, city, street, number)
values (584, 'France', 'Tours', 'Fremont', '38');
insert into pickup_places (id, country, city, street, number)
values (585, 'Vietnam', 'Thị Trấn Tam Sơn', 'Mitchell', '00068');
insert into pickup_places (id, country, city, street, number)
values (586, 'China', 'Ninghai', 'Arapahoe', '37723');
insert into pickup_places (id, country, city, street, number)
values (587, 'South Sudan', 'Raga', 'Northland', '3052');
insert into pickup_places (id, country, city, street, number)
values (588, 'China', 'Wenwucao', 'Arkansas', '2');
insert into pickup_places (id, country, city, street, number)
values (589, 'Japan', 'Katsuta', 'Straubel', '38432');
insert into pickup_places (id, country, city, street, number)
values (590, 'Indonesia', 'Sukamulya', 'Maryland', '06');
insert into pickup_places (id, country, city, street, number)
values (591, 'Japan', 'Toyohama', 'Johnson', '9');
insert into pickup_places (id, country, city, street, number)
values (592, 'Philippines', 'Victoria', 'Truax', '520');
insert into pickup_places (id, country, city, street, number)
values (593, 'Philippines', 'Monkayo', 'Walton', '015');
insert into pickup_places (id, country, city, street, number)
values (594, 'Sweden', 'Falun', 'Arizona', '82');
insert into pickup_places (id, country, city, street, number)
values (595, 'Yemen', 'Al Misrākh', 'Old Shore', '62');
insert into pickup_places (id, country, city, street, number)
values (596, 'Papua New Guinea', 'Kavieng', 'Monica', '485');
insert into pickup_places (id, country, city, street, number)
values (597, 'Poland', 'Książki', 'Basil', '28562');
insert into pickup_places (id, country, city, street, number)
values (598, 'Indonesia', 'Pagelaran', 'Grayhawk', '5846');
insert into pickup_places (id, country, city, street, number)
values (599, 'Mexico', 'San Isidro', 'Browning', '2');
insert into pickup_places (id, country, city, street, number)
values (600, 'Uganda', 'Pader Palwo', 'Bobwhite', '86');
insert into pickup_places (id, country, city, street, number)
values (601, 'China', 'Liangcunchang', 'Transport', '03');
insert into pickup_places (id, country, city, street, number)
values (602, 'Colombia', 'Planeta Rica', 'Monterey', '07');
insert into pickup_places (id, country, city, street, number)
values (603, 'Armenia', 'Lukashin', 'Fuller', '668');
insert into pickup_places (id, country, city, street, number)
values (604, 'Sweden', 'Helsingborg', 'Ridgeway', '73');
insert into pickup_places (id, country, city, street, number)
values (605, 'Vietnam', 'Đắk Song', 'Sunfield', '3');
insert into pickup_places (id, country, city, street, number)
values (606, 'Indonesia', 'Dadaha', 'Orin', '7');
insert into pickup_places (id, country, city, street, number)
values (607, 'Senegal', 'Thiès Nones', 'Maryland', '929');
insert into pickup_places (id, country, city, street, number)
values (608, 'Peru', 'Nepeña', 'Toban', '2489');
insert into pickup_places (id, country, city, street, number)
values (609, 'Japan', 'Kazuno', 'Linden', '75470');
insert into pickup_places (id, country, city, street, number)
values (610, 'Indonesia', 'Silodakon', 'Ohio', '169');
insert into pickup_places (id, country, city, street, number)
values (611, 'Indonesia', 'Sinjai', 'Esch', '4');
insert into pickup_places (id, country, city, street, number)
values (612, 'China', 'Dabachang', 'Paget', '781');
insert into pickup_places (id, country, city, street, number)
values (613, 'Ethiopia', 'Maych’ew', 'Lillian', '3202');
insert into pickup_places (id, country, city, street, number)
values (614, 'Philippines', 'Mindupok', 'Pennsylvania', '406');
insert into pickup_places (id, country, city, street, number)
values (615, 'China', 'Jiangqiao', 'Shoshone', '59391');
insert into pickup_places (id, country, city, street, number)
values (616, 'Russia', 'Nerekhta', 'Bobwhite', '5197');
insert into pickup_places (id, country, city, street, number)
values (617, 'Indonesia', 'Pagarbatu', 'Holmberg', '7');
insert into pickup_places (id, country, city, street, number)
values (618, 'China', 'Beizi', 'Ryan', '9368');
insert into pickup_places (id, country, city, street, number)
values (619, 'China', 'Liuliping', 'Mccormick', '257');
insert into pickup_places (id, country, city, street, number)
values (620, 'Morocco', 'Rabat', 'Gateway', '907');
insert into pickup_places (id, country, city, street, number)
values (621, 'Vietnam', 'Thanh Nê', 'Birchwood', '735');
insert into pickup_places (id, country, city, street, number)
values (622, 'Philippines', 'Santo Domingo', 'Southridge', '97142');
insert into pickup_places (id, country, city, street, number)
values (623, 'Morocco', 'Oulmes', 'Burning Wood', '89970');
insert into pickup_places (id, country, city, street, number)
values (624, 'Uzbekistan', 'Salor', 'Ludington', '7');
insert into pickup_places (id, country, city, street, number)
values (625, 'Brazil', 'Três Pontas', 'Pennsylvania', '22');
insert into pickup_places (id, country, city, street, number)
values (626, 'Chile', 'Putre', 'Kinsman', '8681');
insert into pickup_places (id, country, city, street, number)
values (627, 'China', 'Xingou', 'Northwestern', '4');
insert into pickup_places (id, country, city, street, number)
values (628, 'Philippines', 'Taltal', 'Di Loreto', '9145');
insert into pickup_places (id, country, city, street, number)
values (629, 'Poland', 'Sadkowice', 'Thierer', '5');
insert into pickup_places (id, country, city, street, number)
values (630, 'China', 'Huangcaotuo', 'Ohio', '3152');
insert into pickup_places (id, country, city, street, number)
values (631, 'Czech Republic', 'Francova Lhota', 'Fair Oaks', '48182');
insert into pickup_places (id, country, city, street, number)
values (632, 'Nicaragua', 'Puerto Morazán', 'Glacier Hill', '05092');
insert into pickup_places (id, country, city, street, number)
values (633, 'Indonesia', 'Talon', 'Southridge', '51');
insert into pickup_places (id, country, city, street, number)
values (634, 'Thailand', 'Wang Sai Phun', 'Paget', '97607');
insert into pickup_places (id, country, city, street, number)
values (635, 'Indonesia', 'Barat', 'Moose', '3659');
insert into pickup_places (id, country, city, street, number)
values (636, 'Afghanistan', 'Khanabad', 'Debs', '521');
insert into pickup_places (id, country, city, street, number)
values (637, 'Indonesia', 'Muaralabuh', 'Helena', '4806');
insert into pickup_places (id, country, city, street, number)
values (638, 'France', 'Vichy', 'Doe Crossing', '6718');
insert into pickup_places (id, country, city, street, number)
values (639, 'Mexico', 'Obrera', 'Gateway', '2779');
insert into pickup_places (id, country, city, street, number)
values (640, 'Mongolia', 'Mandal', 'Rusk', '08');
insert into pickup_places (id, country, city, street, number)
values (641, 'China', 'Daying', 'Porter', '9190');
insert into pickup_places (id, country, city, street, number)
values (642, 'France', 'Avignon', 'Brentwood', '97');
insert into pickup_places (id, country, city, street, number)
values (643, 'Albania', 'Ballsh', 'Sutteridge', '725');
insert into pickup_places (id, country, city, street, number)
values (644, 'Mexico', 'Lindavista', 'Parkside', '23');
insert into pickup_places (id, country, city, street, number)
values (645, 'United States', 'Sacramento', 'Vernon', '39');
insert into pickup_places (id, country, city, street, number)
values (646, 'Vietnam', 'Hậu Nghĩa', 'High Crossing', '418');
insert into pickup_places (id, country, city, street, number)
values (647, 'Indonesia', 'Genyem', 'Mifflin', '655');
insert into pickup_places (id, country, city, street, number)
values (648, 'Uruguay', 'Young', 'Bunting', '29');
insert into pickup_places (id, country, city, street, number)
values (649, 'Albania', 'Përmet', 'West', '15696');
insert into pickup_places (id, country, city, street, number)
values (650, 'Mexico', 'Zaragoza', 'Hanson', '31');
insert into pickup_places (id, country, city, street, number)
values (651, 'Philippines', 'Kabacan', 'Hayes', '5792');
insert into pickup_places (id, country, city, street, number)
values (652, 'China', 'Sizhou', 'Iowa', '9');
insert into pickup_places (id, country, city, street, number)
values (653, 'Philippines', 'Manoc-Manoc', 'Blackbird', '920');
insert into pickup_places (id, country, city, street, number)
values (654, 'France', 'Saint-Priest', 'La Follette', '4455');
insert into pickup_places (id, country, city, street, number)
values (655, 'Ukraine', 'Troyits’ke', 'Hauk', '33');
insert into pickup_places (id, country, city, street, number)
values (656, 'Indonesia', 'Tumpang Satu', '2nd', '0980');
insert into pickup_places (id, country, city, street, number)
values (657, 'China', 'Fuying', 'La Follette', '8');
insert into pickup_places (id, country, city, street, number)
values (658, 'Russia', 'Novoaleksandrovsk', 'Onsgard', '371');
insert into pickup_places (id, country, city, street, number)
values (659, 'China', 'Zhuyang', 'Forest Dale', '49379');
insert into pickup_places (id, country, city, street, number)
values (660, 'Indonesia', 'Tegalsari', 'Delladonna', '1449');
insert into pickup_places (id, country, city, street, number)
values (661, 'China', 'Baijiang', 'Eagan', '35137');
insert into pickup_places (id, country, city, street, number)
values (662, 'China', 'Danzao', 'Merry', '6');
insert into pickup_places (id, country, city, street, number)
values (663, 'China', 'Huangzhawan', 'Dapin', '66372');
insert into pickup_places (id, country, city, street, number)
values (664, 'France', 'Vesoul', 'Green Ridge', '0232');
insert into pickup_places (id, country, city, street, number)
values (665, 'Poland', 'Niwiska', 'Pawling', '4');
insert into pickup_places (id, country, city, street, number)
values (666, 'China', 'Jiefang', 'Nelson', '14');
insert into pickup_places (id, country, city, street, number)
values (667, 'Democratic Republic of the Congo', 'Bondo', 'Macpherson', '9');
insert into pickup_places (id, country, city, street, number)
values (668, 'Armenia', 'Verin Dvin', 'Arizona', '22131');
insert into pickup_places (id, country, city, street, number)
values (669, 'Slovenia', 'Zgornje Pirniče', 'Mariners Cove', '3116');
insert into pickup_places (id, country, city, street, number)
values (670, 'Germany', 'München', 'Cascade', '475');
insert into pickup_places (id, country, city, street, number)
values (671, 'France', 'Béziers', 'Jana', '838');
insert into pickup_places (id, country, city, street, number)
values (672, 'China', 'Shuangshipu', 'Declaration', '129');
insert into pickup_places (id, country, city, street, number)
values (673, 'Poland', 'Lyski', 'Michigan', '84');
insert into pickup_places (id, country, city, street, number)
values (674, 'United States', 'Orlando', 'Russell', '98974');
insert into pickup_places (id, country, city, street, number)
values (675, 'China', 'Fenglin', 'Stephen', '87807');
insert into pickup_places (id, country, city, street, number)
values (676, 'Russia', 'Kushelevka', 'David', '4492');
insert into pickup_places (id, country, city, street, number)
values (677, 'Uganda', 'Kiryandongo', 'Colorado', '2054');
insert into pickup_places (id, country, city, street, number)
values (678, 'Brazil', 'Tupanciretã', 'Londonderry', '43179');
insert into pickup_places (id, country, city, street, number)
values (679, 'Russia', 'Yamkino', 'Morrow', '99');
insert into pickup_places (id, country, city, street, number)
values (680, 'Indonesia', 'Sepit', 'Milwaukee', '061');
insert into pickup_places (id, country, city, street, number)
values (681, 'Brazil', 'Cataguases', 'Becker', '6');
insert into pickup_places (id, country, city, street, number)
values (682, 'Portugal', 'Figueiredo', 'Veith', '0294');
insert into pickup_places (id, country, city, street, number)
values (683, 'Indonesia', 'Cikondang', 'Killdeer', '94552');
insert into pickup_places (id, country, city, street, number)
values (684, 'China', 'Xinji', 'Waywood', '6');
insert into pickup_places (id, country, city, street, number)
values (685, 'Indonesia', 'Pangatikan', 'Brown', '6261');
insert into pickup_places (id, country, city, street, number)
values (686, 'Philippines', 'Lumbayan', 'Twin Pines', '219');
insert into pickup_places (id, country, city, street, number)
values (687, 'Georgia', 'Lanchkhuti', 'Kenwood', '706');
insert into pickup_places (id, country, city, street, number)
values (688, 'Greece', 'Pigí', 'Northridge', '25');
insert into pickup_places (id, country, city, street, number)
values (689, 'Madagascar', 'Antsohimbondrona', '8th', '1530');
insert into pickup_places (id, country, city, street, number)
values (690, 'Indonesia', 'Mojogajeh', 'Shopko', '05712');
insert into pickup_places (id, country, city, street, number)
values (691, 'Albania', 'Librazhd', 'Quincy', '153');
insert into pickup_places (id, country, city, street, number)
values (692, 'Morocco', 'Moulay Abdallah', 'Harper', '5130');
insert into pickup_places (id, country, city, street, number)
values (693, 'Uganda', 'Kabale', 'Knutson', '586');
insert into pickup_places (id, country, city, street, number)
values (694, 'Syria', 'Banān', 'Canary', '60778');
insert into pickup_places (id, country, city, street, number)
values (695, 'China', 'Danyang', 'Donald', '37');
insert into pickup_places (id, country, city, street, number)
values (696, 'Afghanistan', 'Tsapêraī', 'Havey', '0695');
insert into pickup_places (id, country, city, street, number)
values (697, 'Albania', 'Libofshë', 'Nevada', '50');
insert into pickup_places (id, country, city, street, number)
values (698, 'France', 'Clermont-Ferrand', 'Lakewood Gardens', '2');
insert into pickup_places (id, country, city, street, number)
values (699, 'Czech Republic', 'Rudolfov', 'Aberg', '16');
insert into pickup_places (id, country, city, street, number)
values (700, 'China', 'Wenxing', 'Linden', '3');
insert into pickup_places (id, country, city, street, number)
values (701, 'China', 'Jiangpu', 'Stone Corner', '566');
insert into pickup_places (id, country, city, street, number)
values (702, 'Indonesia', 'Kool Tengah', 'Little Fleur', '33426');
insert into pickup_places (id, country, city, street, number)
values (703, 'Japan', 'Ōmachi', 'Johnson', '154');
insert into pickup_places (id, country, city, street, number)
values (704, 'China', 'Dongshan', 'Mendota', '11485');
insert into pickup_places (id, country, city, street, number)
values (705, 'Sweden', 'Stockholm', 'Golden Leaf', '7771');
insert into pickup_places (id, country, city, street, number)
values (706, 'Afghanistan', 'Dū Qalah', 'Meadow Valley', '84444');
insert into pickup_places (id, country, city, street, number)
values (707, 'Brazil', 'Rancharia', 'Texas', '6633');
insert into pickup_places (id, country, city, street, number)
values (708, 'Russia', 'Pesochnoye', 'Delladonna', '0019');
insert into pickup_places (id, country, city, street, number)
values (709, 'United States', 'Portland', 'Eagan', '370');
insert into pickup_places (id, country, city, street, number)
values (710, 'Georgia', 'Marneuli', 'Sunnyside', '35836');
insert into pickup_places (id, country, city, street, number)
values (711, 'China', 'Hejia', 'Forest Dale', '56');
insert into pickup_places (id, country, city, street, number)
values (712, 'Philippines', 'Guimbal', 'Vera', '5');
insert into pickup_places (id, country, city, street, number)
values (713, 'Afghanistan', 'Karukh', 'Johnson', '4527');
insert into pickup_places (id, country, city, street, number)
values (714, 'China', 'Nianpan', 'Michigan', '64');
insert into pickup_places (id, country, city, street, number)
values (715, 'Sweden', 'Älvängen', 'Magdeline', '1');
insert into pickup_places (id, country, city, street, number)
values (716, 'Philippines', 'Malhiao', 'Lighthouse Bay', '4457');
insert into pickup_places (id, country, city, street, number)
values (717, 'Russia', 'Ordynskoye', 'Crest Line', '6');
insert into pickup_places (id, country, city, street, number)
values (718, 'Czech Republic', 'Malé Svatoňovice', 'Waywood', '9');
insert into pickup_places (id, country, city, street, number)
values (719, 'Russia', 'Saint Petersburg', 'Fulton', '7771');
insert into pickup_places (id, country, city, street, number)
values (720, 'Finland', 'Muhos', 'Vidon', '1');
insert into pickup_places (id, country, city, street, number)
values (721, 'France', 'Rennes', 'Drewry', '79455');
insert into pickup_places (id, country, city, street, number)
values (722, 'Portugal', 'Gavião', 'Morningstar', '22809');
insert into pickup_places (id, country, city, street, number)
values (723, 'Indonesia', 'Sobontoro', 'Shasta', '227');
insert into pickup_places (id, country, city, street, number)
values (724, 'Venezuela', 'Bachaquero', 'Melvin', '58');
insert into pickup_places (id, country, city, street, number)
values (725, 'Brazil', 'Nova Petrópolis', 'Farwell', '834');
insert into pickup_places (id, country, city, street, number)
values (726, 'France', 'Orvault', 'Toban', '70');
insert into pickup_places (id, country, city, street, number)
values (727, 'Greece', 'Examília', '3rd', '7');
insert into pickup_places (id, country, city, street, number)
values (728, 'Russia', 'Yubileynyy', 'Cody', '73');
insert into pickup_places (id, country, city, street, number)
values (729, 'Philippines', 'Sagay', 'Vernon', '1');
insert into pickup_places (id, country, city, street, number)
values (730, 'Indonesia', 'Parigi', 'Ridge Oak', '2643');
insert into pickup_places (id, country, city, street, number)
values (731, 'United States', 'Philadelphia', 'Judy', '2');
insert into pickup_places (id, country, city, street, number)
values (732, 'China', 'Xincun', 'Russell', '183');
insert into pickup_places (id, country, city, street, number)
values (733, 'Indonesia', 'Puamata', 'Schmedeman', '5');
insert into pickup_places (id, country, city, street, number)
values (734, 'China', 'Zhuanqukou', 'Shelley', '984');
insert into pickup_places (id, country, city, street, number)
values (735, 'Syria', 'Awaj', '2nd', '624');
insert into pickup_places (id, country, city, street, number)
values (736, 'Poland', 'Chełmiec', '8th', '598');
insert into pickup_places (id, country, city, street, number)
values (737, 'Myanmar', 'Wakema', 'Bobwhite', '35');
insert into pickup_places (id, country, city, street, number)
values (738, 'Poland', 'Sokolniki', 'Moose', '71285');
insert into pickup_places (id, country, city, street, number)
values (739, 'Palestinian Territory', 'Bayt Maqdūm', 'Walton', '03522');
insert into pickup_places (id, country, city, street, number)
values (740, 'Poland', 'Łaszczów', 'Arizona', '60452');
insert into pickup_places (id, country, city, street, number)
values (741, 'Philippines', 'San Miguel', 'Del Mar', '2');
insert into pickup_places (id, country, city, street, number)
values (742, 'Russia', 'Karabudakhkent', 'Helena', '1063');
insert into pickup_places (id, country, city, street, number)
values (743, 'France', 'Vittel', 'Del Sol', '1');
insert into pickup_places (id, country, city, street, number)
values (744, 'Nigeria', 'Dankama', 'Marcy', '97730');
insert into pickup_places (id, country, city, street, number)
values (745, 'Argentina', 'Salta', 'Veith', '40');
insert into pickup_places (id, country, city, street, number)
values (746, 'Ukraine', 'Vinnytsya', 'Goodland', '6');
insert into pickup_places (id, country, city, street, number)
values (747, 'Croatia', 'Peteranec', 'Jackson', '9452');
insert into pickup_places (id, country, city, street, number)
values (748, 'Philippines', 'Pandan Niog', 'Russell', '8486');
insert into pickup_places (id, country, city, street, number)
values (749, 'China', 'Tianshan', 'Golf View', '98651');
insert into pickup_places (id, country, city, street, number)
values (750, 'Finland', 'Turku', 'Park Meadow', '5');
insert into pickup_places (id, country, city, street, number)
values (751, 'Netherlands', 'Breda', 'Hanover', '68');
insert into pickup_places (id, country, city, street, number)
values (752, 'Afghanistan', 'Dasht-e Archī', 'Carioca', '70765');
insert into pickup_places (id, country, city, street, number)
values (753, 'Portugal', 'Portela', 'Delaware', '1944');
insert into pickup_places (id, country, city, street, number)
values (754, 'Nicaragua', 'La Paz de Oriente', 'Pine View', '28155');
insert into pickup_places (id, country, city, street, number)
values (755, 'Malaysia', 'Puchong', 'Iowa', '32741');
insert into pickup_places (id, country, city, street, number)
values (756, 'Vietnam', 'Bình Thủy', 'Briar Crest', '5382');
insert into pickup_places (id, country, city, street, number)
values (757, 'China', 'Hulu', 'Anhalt', '84');
insert into pickup_places (id, country, city, street, number)
values (758, 'Ireland', 'Fermoy', 'Warbler', '32');
insert into pickup_places (id, country, city, street, number)
values (759, 'Indonesia', 'Pajaten', 'Tony', '6');
insert into pickup_places (id, country, city, street, number)
values (760, 'China', 'Xinquan', 'Amoth', '6059');
insert into pickup_places (id, country, city, street, number)
values (761, 'Albania', 'Saraqinishtë', 'Lerdahl', '3');
insert into pickup_places (id, country, city, street, number)
values (762, 'Indonesia', 'Montasik', 'Meadow Ridge', '0655');
insert into pickup_places (id, country, city, street, number)
values (763, 'Indonesia', 'Pepayan', 'Springs', '8');
insert into pickup_places (id, country, city, street, number)
values (764, 'Iran', 'Borūjerd', 'Declaration', '9760');
insert into pickup_places (id, country, city, street, number)
values (765, 'Armenia', 'Arshaluys', 'Linden', '0337');
insert into pickup_places (id, country, city, street, number)
values (766, 'South Africa', 'Carletonville', 'Arrowood', '487');
insert into pickup_places (id, country, city, street, number)
values (767, 'Indonesia', 'Mlonggo', 'Carey', '93');
insert into pickup_places (id, country, city, street, number)
values (768, 'Poland', 'Krosno Odrzańskie', 'Delaware', '2');
insert into pickup_places (id, country, city, street, number)
values (769, 'Sweden', 'Enskededalen', 'Arkansas', '9');
insert into pickup_places (id, country, city, street, number)
values (770, 'Philippines', 'Pung-Pang', 'Crownhardt', '18054');
insert into pickup_places (id, country, city, street, number)
values (771, 'Japan', 'Asahikawa', 'Kennedy', '055');
insert into pickup_places (id, country, city, street, number)
values (772, 'Russia', 'Nelidovo', 'Hanson', '999');
insert into pickup_places (id, country, city, street, number)
values (773, 'Indonesia', 'Wairinding', 'Bellgrove', '5');
insert into pickup_places (id, country, city, street, number)
values (774, 'France', 'Aurillac', '5th', '66138');
insert into pickup_places (id, country, city, street, number)
values (775, 'Indonesia', 'Haruai', 'Lukken', '30801');
insert into pickup_places (id, country, city, street, number)
values (776, 'China', 'Deqing', 'Old Gate', '5606');
insert into pickup_places (id, country, city, street, number)
values (777, 'Kazakhstan', 'Lenīnskīy', 'Clarendon', '7');
insert into pickup_places (id, country, city, street, number)
values (778, 'China', 'Gaoqiu', 'South', '45081');
insert into pickup_places (id, country, city, street, number)
values (779, 'China', 'Zhoujiaba', 'Stephen', '0');
insert into pickup_places (id, country, city, street, number)
values (780, 'Philippines', 'San Francisco', 'Vera', '497');
insert into pickup_places (id, country, city, street, number)
values (781, 'Japan', 'Kikuchi', 'Reinke', '4867');
insert into pickup_places (id, country, city, street, number)
values (782, 'Madagascar', 'Ankazoabo', 'Anthes', '5');
insert into pickup_places (id, country, city, street, number)
values (783, 'China', 'Madoi', 'Hayes', '41448');
insert into pickup_places (id, country, city, street, number)
values (784, 'Poland', 'Września', 'Fieldstone', '9642');
insert into pickup_places (id, country, city, street, number)
values (785, 'Russia', 'Lensk', 'South', '9');
insert into pickup_places (id, country, city, street, number)
values (786, 'Russia', 'Tankhoy', 'Ridge Oak', '6');
insert into pickup_places (id, country, city, street, number)
values (787, 'Portugal', 'Portela', 'Montana', '9');
insert into pickup_places (id, country, city, street, number)
values (788, 'Indonesia', 'Cidikit Girang', 'Kim', '96100');
insert into pickup_places (id, country, city, street, number)
values (789, 'China', 'Kedian', 'Susan', '5511');
insert into pickup_places (id, country, city, street, number)
values (790, 'Russia', 'Pavlovsk', 'Ridgeview', '21');
insert into pickup_places (id, country, city, street, number)
values (791, 'Somalia', 'Lughaye', 'Sage', '1722');
insert into pickup_places (id, country, city, street, number)
values (792, 'Guatemala', 'San Juan Ermita', 'Amoth', '7947');
insert into pickup_places (id, country, city, street, number)
values (793, 'France', 'Saint-Amand-les-Eaux', 'Troy', '02114');
insert into pickup_places (id, country, city, street, number)
values (794, 'Mali', 'Mopti', 'Helena', '20');
insert into pickup_places (id, country, city, street, number)
values (795, 'Mexico', 'La Loma', 'Texas', '94');
insert into pickup_places (id, country, city, street, number)
values (796, 'France', 'Paris La Défense', 'Hallows', '65');
insert into pickup_places (id, country, city, street, number)
values (797, 'Brazil', 'São Francisco do Sul', 'Cottonwood', '502');
insert into pickup_places (id, country, city, street, number)
values (798, 'Indonesia', 'Patrang', 'Superior', '0719');
insert into pickup_places (id, country, city, street, number)
values (799, 'China', 'Jianguang', 'Towne', '20');
insert into pickup_places (id, country, city, street, number)
values (800, 'Indonesia', 'Semerak', 'Garrison', '417');
insert into pickup_places (id, country, city, street, number)
values (801, 'Russia', 'Североморск-3', 'Straubel', '5');
insert into pickup_places (id, country, city, street, number)
values (802, 'Indonesia', 'Tegalloa', 'Vermont', '923');
insert into pickup_places (id, country, city, street, number)
values (803, 'Portugal', 'Quinta dos Frades', 'Thierer', '38');
insert into pickup_places (id, country, city, street, number)
values (804, 'Haiti', 'Torbeck', 'Eagle Crest', '18');
insert into pickup_places (id, country, city, street, number)
values (805, 'Cameroon', 'Batouri', 'Nancy', '250');
insert into pickup_places (id, country, city, street, number)
values (806, 'Philippines', 'Suba', 'Continental', '50');
insert into pickup_places (id, country, city, street, number)
values (807, 'China', 'Yongjiang', 'Annamark', '036');
insert into pickup_places (id, country, city, street, number)
values (808, 'Syria', 'Nāḩiyat as Sab Biyār', 'Garrison', '1');
insert into pickup_places (id, country, city, street, number)
values (809, 'United States', 'Columbus', 'Scott', '717');
insert into pickup_places (id, country, city, street, number)
values (810, 'Philippines', 'Mianay', 'Elgar', '1549');
insert into pickup_places (id, country, city, street, number)
values (811, 'China', 'Shangchewan', 'Carpenter', '27');
insert into pickup_places (id, country, city, street, number)
values (812, 'Russia', 'Kysyl-Syr', 'Susan', '524');
insert into pickup_places (id, country, city, street, number)
values (813, 'Nigeria', 'Dikwa', 'Monterey', '821');
insert into pickup_places (id, country, city, street, number)
values (814, 'Indonesia', 'Tiang', 'Ridgeview', '72529');
insert into pickup_places (id, country, city, street, number)
values (815, 'Greece', 'Kallithéa', 'Jay', '2');
insert into pickup_places (id, country, city, street, number)
values (816, 'Indonesia', 'Lueng Putu', 'Drewry', '49269');
insert into pickup_places (id, country, city, street, number)
values (817, 'China', 'Wuyang', 'Northwestern', '9989');
insert into pickup_places (id, country, city, street, number)
values (818, 'Mongolia', 'Suugaant', 'Kinsman', '16');
insert into pickup_places (id, country, city, street, number)
values (819, 'Nigeria', 'Dan Sadau', 'Cottonwood', '3107');
insert into pickup_places (id, country, city, street, number)
values (820, 'Philippines', 'Sigma', 'Dapin', '917');
insert into pickup_places (id, country, city, street, number)
values (821, 'Indonesia', 'Krajan Joho', 'Warner', '3');
insert into pickup_places (id, country, city, street, number)
values (822, 'Indonesia', 'Panenjoan', 'Luster', '42588');
insert into pickup_places (id, country, city, street, number)
values (823, 'Indonesia', 'Trenggulunan', 'Waywood', '8933');
insert into pickup_places (id, country, city, street, number)
values (824, 'Peru', 'Sojo', 'Sloan', '268');
insert into pickup_places (id, country, city, street, number)
values (825, 'Thailand', 'Ban Phan Don', 'Scott', '26205');
insert into pickup_places (id, country, city, street, number)
values (826, 'China', 'Jinshandian', 'Mallory', '5');
insert into pickup_places (id, country, city, street, number)
values (827, 'Indonesia', 'Maunuri', 'Barnett', '207');
insert into pickup_places (id, country, city, street, number)
values (828, 'Indonesia', 'Pasolapida', 'Anderson', '818');
insert into pickup_places (id, country, city, street, number)
values (829, 'Egypt', 'Aş Şaff', 'Golf Course', '4');
insert into pickup_places (id, country, city, street, number)
values (830, 'Poland', 'Sucha', 'Darwin', '6478');
insert into pickup_places (id, country, city, street, number)
values (831, 'Ukraine', 'Kuchurhan', 'Spaight', '987');
insert into pickup_places (id, country, city, street, number)
values (832, 'Belarus', 'Lyntupy', 'Carioca', '3121');
insert into pickup_places (id, country, city, street, number)
values (833, 'China', 'Hekou', 'Merchant', '6062');
insert into pickup_places (id, country, city, street, number)
values (834, 'Colombia', 'La Unión', 'Eastlawn', '55');
insert into pickup_places (id, country, city, street, number)
values (835, 'China', 'Nancheng', 'Waxwing', '9');
insert into pickup_places (id, country, city, street, number)
values (836, 'China', 'Gensi', 'Petterle', '6');
insert into pickup_places (id, country, city, street, number)
values (837, 'Philippines', 'Toong', 'Blue Bill Park', '5156');
insert into pickup_places (id, country, city, street, number)
values (838, 'China', 'Renxian', 'Forest Run', '6453');
insert into pickup_places (id, country, city, street, number)
values (839, 'Nigeria', 'Orodo', 'Kropf', '16');
insert into pickup_places (id, country, city, street, number)
values (840, 'Greece', 'Ágios Andréas', 'Swallow', '95152');
insert into pickup_places (id, country, city, street, number)
values (841, 'Nigeria', 'Tokombere', 'Sycamore', '021');
insert into pickup_places (id, country, city, street, number)
values (842, 'Argentina', 'Arroyito', 'Blackbird', '94673');
insert into pickup_places (id, country, city, street, number)
values (843, 'France', 'Évreux', 'Bultman', '223');
insert into pickup_places (id, country, city, street, number)
values (844, 'Canada', 'Mattawa', 'Hoard', '5074');
insert into pickup_places (id, country, city, street, number)
values (845, 'Egypt', 'Al Bawīţī', 'Merrick', '68572');
insert into pickup_places (id, country, city, street, number)
values (846, 'China', 'Cairima', 'Waywood', '3042');
insert into pickup_places (id, country, city, street, number)
values (847, 'Indonesia', 'Sikur', 'Lyons', '070');
insert into pickup_places (id, country, city, street, number)
values (848, 'Portugal', 'Sebadelhe', 'Larry', '8');
insert into pickup_places (id, country, city, street, number)
values (849, 'France', 'Nantes', 'New Castle', '62239');
insert into pickup_places (id, country, city, street, number)
values (850, 'Namibia', 'Omaruru', 'Miller', '9');
insert into pickup_places (id, country, city, street, number)
values (851, 'Nigeria', 'Ughelli', 'Maple', '4');
insert into pickup_places (id, country, city, street, number)
values (852, 'China', 'Gongjiahe', 'Northridge', '2');
insert into pickup_places (id, country, city, street, number)
values (853, 'Japan', 'Ōmagari', 'North', '11');
insert into pickup_places (id, country, city, street, number)
values (854, 'United States', 'San Jose', 'Petterle', '704');
insert into pickup_places (id, country, city, street, number)
values (855, 'China', 'Hufeng', 'Meadow Valley', '6');
insert into pickup_places (id, country, city, street, number)
values (856, 'Bulgaria', 'Troyan', 'American Ash', '26');
insert into pickup_places (id, country, city, street, number)
values (857, 'Brazil', 'Senhor do Bonfim', 'Monument', '0');
insert into pickup_places (id, country, city, street, number)
values (858, 'Nigeria', 'Omoku', 'Old Shore', '49770');
insert into pickup_places (id, country, city, street, number)
values (859, 'South Korea', 'Seogeom-ri', 'Bultman', '4');
insert into pickup_places (id, country, city, street, number)
values (860, 'China', 'Gaopu', 'Westridge', '03');
insert into pickup_places (id, country, city, street, number)
values (861, 'Czech Republic', 'Moravská Nová Ves', 'Stephen', '0');
insert into pickup_places (id, country, city, street, number)
values (862, 'France', 'Lyon', 'Melody', '5');
insert into pickup_places (id, country, city, street, number)
values (863, 'Indonesia', 'Krajan Ngrambingan', 'Lotheville', '303');
insert into pickup_places (id, country, city, street, number)
values (864, 'Indonesia', 'Talun', 'Charing Cross', '244');
insert into pickup_places (id, country, city, street, number)
values (865, 'China', 'Taihe', 'Dunning', '301');
insert into pickup_places (id, country, city, street, number)
values (866, 'China', 'Guilin', 'Becker', '388');
insert into pickup_places (id, country, city, street, number)
values (867, 'Russia', 'Kvitok', 'Eastlawn', '26');
insert into pickup_places (id, country, city, street, number)
values (868, 'China', 'Baiyang', 'Anniversary', '89');
insert into pickup_places (id, country, city, street, number)
values (869, 'Sweden', 'Tanumshede', 'Di Loreto', '4');
insert into pickup_places (id, country, city, street, number)
values (870, 'Czech Republic', 'Žeravice', 'International', '0');
insert into pickup_places (id, country, city, street, number)
values (871, 'Philippines', 'Quinipot', 'Cardinal', '5');
insert into pickup_places (id, country, city, street, number)
values (872, 'Brazil', 'Jaguarão', 'Merry', '3481');
insert into pickup_places (id, country, city, street, number)
values (873, 'China', 'Sanjia', 'Heath', '15348');
insert into pickup_places (id, country, city, street, number)
values (874, 'Russia', 'Pokhvistnevo', 'Towne', '6');
insert into pickup_places (id, country, city, street, number)
values (875, 'Kenya', 'Maralal', 'Butternut', '7');
insert into pickup_places (id, country, city, street, number)
values (876, 'China', 'Tong’an', 'Londonderry', '77');
insert into pickup_places (id, country, city, street, number)
values (877, 'Indonesia', 'Pasirtundun', 'Westerfield', '7336');
insert into pickup_places (id, country, city, street, number)
values (878, 'Japan', 'Tanabe', 'Blaine', '36');
insert into pickup_places (id, country, city, street, number)
values (879, 'Finland', 'Joroinen', 'Schlimgen', '145');
insert into pickup_places (id, country, city, street, number)
values (880, 'Sweden', 'Kvissleby', 'Killdeer', '5866');
insert into pickup_places (id, country, city, street, number)
values (881, 'Japan', 'Fukura', 'Esker', '899');
insert into pickup_places (id, country, city, street, number)
values (882, 'China', 'Bor Ondor', 'Messerschmidt', '74');
insert into pickup_places (id, country, city, street, number)
values (883, 'China', 'Shuqiao', 'Parkside', '3193');
insert into pickup_places (id, country, city, street, number)
values (884, 'Indonesia', 'Cigintung', 'Old Shore', '28');
insert into pickup_places (id, country, city, street, number)
values (885, 'South Africa', 'Dewetsdorp', 'Hintze', '3102');
insert into pickup_places (id, country, city, street, number)
values (886, 'Guatemala', 'Barberena', 'Debra', '563');
insert into pickup_places (id, country, city, street, number)
values (887, 'China', 'Duyang', 'Rusk', '651');
insert into pickup_places (id, country, city, street, number)
values (888, 'Russia', 'Orlovskiy', 'Tony', '21928');
insert into pickup_places (id, country, city, street, number)
values (889, 'Russia', 'Ekimchan', 'Michigan', '74');
insert into pickup_places (id, country, city, street, number)
values (890, 'Philippines', 'Santa Fe', 'Kropf', '0');
insert into pickup_places (id, country, city, street, number)
values (891, 'Indonesia', 'Tukbur', 'Emmet', '28');
insert into pickup_places (id, country, city, street, number)
values (892, 'Philippines', 'Davao', 'Farwell', '44');
insert into pickup_places (id, country, city, street, number)
values (893, 'Philippines', 'Masiga', 'Muir', '0');
insert into pickup_places (id, country, city, street, number)
values (894, 'China', 'Banshu', 'Cambridge', '4715');
insert into pickup_places (id, country, city, street, number)
values (895, 'Poland', 'Maniowy', 'Bonner', '44271');
insert into pickup_places (id, country, city, street, number)
values (896, 'Brazil', 'Cajazeiras', 'Aberg', '71123');
insert into pickup_places (id, country, city, street, number)
values (897, 'Brazil', 'Pilar do Sul', 'Aberg', '39633');
insert into pickup_places (id, country, city, street, number)
values (898, 'China', 'Fengshan', 'Sundown', '23');
insert into pickup_places (id, country, city, street, number)
values (899, 'Bosnia and Herzegovina', 'Ključ', 'Dryden', '57');
insert into pickup_places (id, country, city, street, number)
values (900, 'Indonesia', 'Sukaraja', 'Dovetail', '400');
insert into pickup_places (id, country, city, street, number)
values (901, 'France', 'Paris La Défense', 'Randy', '142');
insert into pickup_places (id, country, city, street, number)
values (902, 'Central African Republic', 'Kouango', 'Autumn Leaf', '10');
insert into pickup_places (id, country, city, street, number)
values (903, 'Finland', 'Rautjärvi', 'Calypso', '6515');
insert into pickup_places (id, country, city, street, number)
values (904, 'China', 'Guantang', 'Rockefeller', '0178');
insert into pickup_places (id, country, city, street, number)
values (905, 'Paraguay', 'Villa Elisa', 'Walton', '98');
insert into pickup_places (id, country, city, street, number)
values (906, 'Syria', 'Şalākhid', 'Elmside', '941');
insert into pickup_places (id, country, city, street, number)
values (907, 'Nepal', 'Butwāl', 'Amoth', '5');
insert into pickup_places (id, country, city, street, number)
values (908, 'Comoros', 'Ivouani', 'Upham', '4');
insert into pickup_places (id, country, city, street, number)
values (909, 'Indonesia', 'Limbangan', 'Corben', '4');
insert into pickup_places (id, country, city, street, number)
values (910, 'Poland', 'Świecie nad Osą', 'Texas', '71544');
insert into pickup_places (id, country, city, street, number)
values (911, 'Russia', 'Krasnaya Polyana', 'Harbort', '5');
insert into pickup_places (id, country, city, street, number)
values (912, 'Philippines', 'Dungon', 'Buell', '72');
insert into pickup_places (id, country, city, street, number)
values (913, 'Philippines', 'Opol', 'Chinook', '2');
insert into pickup_places (id, country, city, street, number)
values (914, 'China', 'Tonglin', 'Fulton', '11');
insert into pickup_places (id, country, city, street, number)
values (915, 'Jamaica', 'Richmond', 'Dwight', '4845');
insert into pickup_places (id, country, city, street, number)
values (916, 'China', 'Xinning', 'Corscot', '07');
insert into pickup_places (id, country, city, street, number)
values (917, 'China', 'Qian’an', 'Pond', '82');
insert into pickup_places (id, country, city, street, number)
values (918, 'China', 'Shaxi', 'Almo', '59');
insert into pickup_places (id, country, city, street, number)
values (919, 'Macedonia', 'Ilinden', 'Leroy', '600');
insert into pickup_places (id, country, city, street, number)
values (920, 'Poland', 'Lubichowo', 'Dakota', '82200');
insert into pickup_places (id, country, city, street, number)
values (921, 'Poland', 'Gręboszów', 'Ohio', '5');
insert into pickup_places (id, country, city, street, number)
values (922, 'Philippines', 'Tanjay', 'Fordem', '16');
insert into pickup_places (id, country, city, street, number)
values (923, 'Philippines', 'Imbang', 'Surrey', '0307');
insert into pickup_places (id, country, city, street, number)
values (924, 'Indonesia', 'Bangbayang', 'Main', '28');
insert into pickup_places (id, country, city, street, number)
values (925, 'Czech Republic', 'Jablonec nad Jizerou', 'Holy Cross', '781');
insert into pickup_places (id, country, city, street, number)
values (926, 'China', 'Daping', 'Dexter', '98208');
insert into pickup_places (id, country, city, street, number)
values (927, 'China', 'Putang', 'Heffernan', '8');
insert into pickup_places (id, country, city, street, number)
values (928, 'Netherlands', 'Hoogeveen', 'Trailsway', '37681');
insert into pickup_places (id, country, city, street, number)
values (929, 'United States', 'Fort Worth', 'Gale', '1');
insert into pickup_places (id, country, city, street, number)
values (930, 'Sweden', 'Borås', 'Fuller', '48');
insert into pickup_places (id, country, city, street, number)
values (931, 'Mongolia', 'Sharga', 'Pierstorff', '57');
insert into pickup_places (id, country, city, street, number)
values (932, 'Sweden', 'Västerås', 'Debs', '1467');
insert into pickup_places (id, country, city, street, number)
values (933, 'Finland', 'Pertunmaa', 'Kinsman', '529');
insert into pickup_places (id, country, city, street, number)
values (934, 'Nigeria', 'Gakem', 'Summit', '9436');
insert into pickup_places (id, country, city, street, number)
values (935, 'Portugal', 'Vila Ribeiro', 'Monterey', '74292');
insert into pickup_places (id, country, city, street, number)
values (936, 'Japan', 'Nemuro', 'Nelson', '870');
insert into pickup_places (id, country, city, street, number)
values (937, 'Haiti', 'Abricots', 'Logan', '5275');
insert into pickup_places (id, country, city, street, number)
values (938, 'Argentina', 'Chimbas', 'Rutledge', '679');
insert into pickup_places (id, country, city, street, number)
values (939, 'China', 'Daxing', 'Artisan', '280');
insert into pickup_places (id, country, city, street, number)
values (940, 'Canada', 'Biggar', 'Grasskamp', '8');
insert into pickup_places (id, country, city, street, number)
values (941, 'Indonesia', 'Mulyadadi', 'Calypso', '30831');
insert into pickup_places (id, country, city, street, number)
values (942, 'Botswana', 'Ramotswa', 'Comanche', '41');
insert into pickup_places (id, country, city, street, number)
values (943, 'China', 'Longshan', 'Onsgard', '467');
insert into pickup_places (id, country, city, street, number)
values (944, 'China', 'Yushang', 'Mcbride', '873');
insert into pickup_places (id, country, city, street, number)
values (945, 'Spain', 'Almeria', 'Rutledge', '22');
insert into pickup_places (id, country, city, street, number)
values (946, 'France', 'Lorient', 'Kings', '28190');
insert into pickup_places (id, country, city, street, number)
values (947, 'China', 'Yujia', 'Shopko', '9');
insert into pickup_places (id, country, city, street, number)
values (948, 'Philippines', 'Aramayuan', 'Ludington', '010');
insert into pickup_places (id, country, city, street, number)
values (949, 'China', 'Hongkeli', 'Westport', '4198');
insert into pickup_places (id, country, city, street, number)
values (950, 'Indonesia', 'Mojorejo', 'Debra', '5');
insert into pickup_places (id, country, city, street, number)
values (951, 'China', 'Jiahe', 'Grasskamp', '002');
insert into pickup_places (id, country, city, street, number)
values (952, 'Finland', 'Jäppilä', 'Corscot', '05055');
insert into pickup_places (id, country, city, street, number)
values (953, 'Ethiopia', 'Āsasa', 'Havey', '94');
insert into pickup_places (id, country, city, street, number)
values (954, 'Finland', 'Piippola', 'International', '41658');
insert into pickup_places (id, country, city, street, number)
values (955, 'Philippines', 'Langob', 'Nancy', '0');
insert into pickup_places (id, country, city, street, number)
values (956, 'China', 'Xilanqi', 'Blaine', '042');
insert into pickup_places (id, country, city, street, number)
values (957, 'France', 'Les Sables-d''Olonne', 'Jenna', '82');
insert into pickup_places (id, country, city, street, number)
values (958, 'Portugal', 'Sever do Vouga', 'Petterle', '9');
insert into pickup_places (id, country, city, street, number)
values (959, 'China', 'Chuanbu', 'Thierer', '9');
insert into pickup_places (id, country, city, street, number)
values (960, 'Malaysia', 'Pulau Pinang', 'Hooker', '69');
insert into pickup_places (id, country, city, street, number)
values (961, 'Ukraine', 'Hlyboka', 'Armistice', '5530');
insert into pickup_places (id, country, city, street, number)
values (962, 'Indonesia', 'Cihaladan', 'Mitchell', '587');
insert into pickup_places (id, country, city, street, number)
values (963, 'Indonesia', 'Calingcing', '4th', '68320');
insert into pickup_places (id, country, city, street, number)
values (964, 'Uruguay', 'Las Toscas', 'Spaight', '2935');
insert into pickup_places (id, country, city, street, number)
values (965, 'Indonesia', 'Pitai', 'Helena', '0396');
insert into pickup_places (id, country, city, street, number)
values (966, 'Poland', 'Głogów Małopolski', 'Mendota', '69');
insert into pickup_places (id, country, city, street, number)
values (967, 'Poland', 'Dębno', 'Rusk', '14');
insert into pickup_places (id, country, city, street, number)
values (968, 'Mexico', 'San Jose', 'Paget', '1885');
insert into pickup_places (id, country, city, street, number)
values (969, 'China', 'Beishidian', 'Quincy', '402');
insert into pickup_places (id, country, city, street, number)
values (970, 'Argentina', 'Villa Mercedes', 'Cody', '60');
insert into pickup_places (id, country, city, street, number)
values (971, 'Canada', 'Cornwall', 'Thierer', '555');
insert into pickup_places (id, country, city, street, number)
values (972, 'China', 'Tai’an', 'Monica', '08779');
insert into pickup_places (id, country, city, street, number)
values (973, 'South Africa', 'Parys', 'Glendale', '9176');
insert into pickup_places (id, country, city, street, number)
values (974, 'Philippines', 'Antipolo', 'Union', '4265');
insert into pickup_places (id, country, city, street, number)
values (975, 'Gambia', 'Sankwia', 'Chive', '92');
insert into pickup_places (id, country, city, street, number)
values (976, 'China', 'Miaoqian', 'Summerview', '38739');
insert into pickup_places (id, country, city, street, number)
values (977, 'China', 'Dahe', 'Lukken', '96');
insert into pickup_places (id, country, city, street, number)
values (978, 'Indonesia', 'Bangbayang', 'Jana', '702');
insert into pickup_places (id, country, city, street, number)
values (979, 'United States', 'Waco', 'Tennyson', '98513');
insert into pickup_places (id, country, city, street, number)
values (980, 'Wallis and Futuna', 'Sigavé', 'Old Shore', '2294');
insert into pickup_places (id, country, city, street, number)
values (981, 'Argentina', 'Villa Cañás', 'Sherman', '1079');
insert into pickup_places (id, country, city, street, number)
values (982, 'Yemen', 'Kuaydinah', 'Hansons', '6218');
insert into pickup_places (id, country, city, street, number)
values (983, 'China', 'Dongshe', 'Red Cloud', '17');
insert into pickup_places (id, country, city, street, number)
values (984, 'Malaysia', 'Lahad Datu', 'Autumn Leaf', '37');
insert into pickup_places (id, country, city, street, number)
values (985, 'Russia', 'Novosil’', 'Spenser', '232');
insert into pickup_places (id, country, city, street, number)
values (986, 'South Africa', 'Kakamas', 'Dryden', '71');
insert into pickup_places (id, country, city, street, number)
values (987, 'United States', 'Roanoke', 'Morningstar', '0687');
insert into pickup_places (id, country, city, street, number)
values (988, 'China', 'Hongshan', 'Macpherson', '0036');
insert into pickup_places (id, country, city, street, number)
values (989, 'China', 'Ganchazi', 'Veith', '74554');
insert into pickup_places (id, country, city, street, number)
values (990, 'Argentina', 'Oro Verde', 'Carpenter', '83148');
insert into pickup_places (id, country, city, street, number)
values (991, 'China', 'Yihe', 'Rusk', '5017');
insert into pickup_places (id, country, city, street, number)
values (992, 'Sweden', 'Bålsta', 'Talisman', '653');
insert into pickup_places (id, country, city, street, number)
values (993, 'Indonesia', 'Nunhala', 'Dayton', '07');
insert into pickup_places (id, country, city, street, number)
values (994, 'Tajikistan', 'Vose’', 'Loeprich', '64513');
insert into pickup_places (id, country, city, street, number)
values (995, 'Syria', 'Al Qadmūs', 'Spohn', '06930');
insert into pickup_places (id, country, city, street, number)
values (996, 'Russia', 'Vishnyakovskiye Dachi', 'Heath', '1758');
insert into pickup_places (id, country, city, street, number)
values (997, 'Indonesia', 'Luan Balu', 'Nevada', '583');
insert into pickup_places (id, country, city, street, number)
values (998, 'South Africa', 'Germiston', 'Onsgard', '554');
insert into pickup_places (id, country, city, street, number)
values (999, 'China', 'Saxin', 'Fulton', '8');
insert into pickup_places (id, country, city, street, number)
values (1000, 'Portugal', 'Leceia', 'Morningstar', '3902');
