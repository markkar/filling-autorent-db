INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (1, false, false, false, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (2, false, false, false, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (3, false, false, false, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (4, false, false, false, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (5, false, false, true, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (6, false, false, true, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (7, false, false, true, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (8, false, false, true, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (9, false, true, false, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (10, false, true, false, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (11, false, true, false, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (12, false, true, false, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (13, false, true, true, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (14, false, true, true, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (15, false, true, true, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (16, false, true, true, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (17, true, false, false, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (18, true, false, false, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (19, true, false, false, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (20, true, false, false, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (21, true, false, true, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (22, true, false, true, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (23, true, false, true, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (24, true, false, true, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (25, true, true, false, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (26, true, true, false, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (27, true, true, false, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (28, true, true, false, true, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (29, true, true, true, false, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (30, true, true, true, false, true);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (31, true, true, true, true, false);
INSERT INTO order_demands (id, gps, wifi, own_driver, baby_chair, air_conditioning)
VALUES (32, true, true, true, true, true);
